---
_title_: 1D Detectors in bliss
_author_: Vicente Rey Bakaikoa
_version_: 0.1
_project_: bliss
_date_: 2022/04/22
---

# 1D Detectors in _bliss_

## Introduction

This document is a reflexion on the integration needs and possibilities for 1D detectors in _bliss_. 
This includes **MCA detectors**, **MCA multi-element detectors**, but also other **1D detectors** like, for example
the Dectris Mythen detector. 

The document aims to serve as the basis for discusion (and reference for the development) 
of 1D support in _bliss_. Some of the principles that should drive that development are:

- functionality 

- performance

- integration 

- simple API to add new device types in _bliss_

- specific features integration



## The _bliss_ view:  General support and Features (or what _bliss_ should implement)

#### 1D Data
All 1D detectors are characterized by the fact that they produce data in the form of 1D arrays (one or several of
them). 

#### Metadata
Each acquisition cycle can have associated metadata. This can include, for example, different statistics for
MCA like detectors. Full metadata must be systematically saved with frame data.  Metadata description should be
provided by the detector implementation as a list of string IDs. Data events produced during the acquisition 
will contain metadata values solely as data arrays.

#### Associated counters
They can all produce a number of counter (0D) values for each acquisition cycle.  These counters are varied in
nature and meaning: ROI sums, acquisition statistics or other. 

   * Detector counters. 
        They can be statistical or other values produced by detector
        possible values declared through `detector.get_counter_description()`
        counter data provided with data events

   * ROIs on one element. 
        Defined with `[ element_no, from, to, calib_flag ]`

   * ROIs through all elements (in multi-element detectors). 
        Defined with `[ "all", from, to, calib_flag ]`

   * Total Summed value of spectra for element `n`.
        this is a particular type of ROI where from, to correspond
        to total range.  Calibration makes no sense in this case.

Detector counters will always be defined, the user could then enable or disable them 
during acquisition. 

ROI counters, including total-sum ROIs, will be defined in beacon. 

Any counter can be enabled/disabled in _bliss_ after being defined.

A desirable feature would be to be able to graphically select ROIs (in _flint_?)

!!!note
    some detector counters may correspond to metadata values, but the 
    developer may decide which, within the metadata values, 
    can acquire the category of counters. This could dramatically reduce the number 
    of automatic counters. For example, for multi-element 
    detectors, if all metadata is selectable as counters the large amount 
    of counters may be quickly become confusing and unmanageable for the user. 
    In that case most of the statistical data would anyway never be used as counter. 
    Still, it may be interesting to always save them as metadata.


#### Calibration

As 1D data array can be considered as a series of measurement channels it is possible to associate channels to a
certain experimental value like distance, time, energy or other. This is done through the use of a calibration
system that could allow to associate data (or ROIs) to those experimental values.

In the case of MCA multi element detectors, calibration may be different for each element.

Calibration is a feature set that should be handled enterely by the 1D detector support in _bliss_, no need to develop
detector plugin support for calibration. 

Possible features: 
   - calibration may be defined in "beacon" 
           / lookup table?  polynomial equation ? - in the first case, calibration kept in files

   - ROI ranges could be defined to use calibration units instead of index units
   - It should be possible to change/save the detector calibration
   - If calibration exists, it should be saved with the acquisition metadata

#### Step by step scans

Prepare acquisition:
*trigger mode as selected*
```python
detector.set_preset_value( acq_time )
```

for each point:

```python
gevent.spawn( detector.trigger() )  
```

The `trigger()` method of the detector will `start_acquisition()` and run while 
the acquisition `is_acquiring()`. 
While it is running it should produce `data` events (with `completed` flag as `False`).
At the end of the acquisition it will produce a final `data` event (with `completed` flag as `True`)

A default implementation could be provided in the 1D base controller class. 
It could be something similar to:

!!!example
```python
   def trigger(self):
       self.start_acquisition()

       while self.is_acquiring():
           data, metadata, detcnts = self.read_data()
           event.emit("data", (data, metadata, detcnts, {"completed":False}))
       finally:
           data, metadata, detcnts = self.read_data()
           event.emit("data", (data, metadata, detcnts, {"completed":True}))
           self.stop_acquisition()
```

That implementation could be overriden by the detector implementation.

#### Fast acquisition scans (trigger=SYNC or GATE)

Most of the hardware used at the ESRF for 1D detectors allow for fast acquisition modes, through some hardware
gating/triggering method, buffering system, etc.. 

In this case the sequence in _bliss_ may look like: 

Prepare acquisition:
*trigger mode as selected*
```python
detector.set_number_points(points)
```

Start acquisition:
```python
detector.start_acquisition()
task = gevent.spawn( detector.update_read_data )
task.link( detector.stop_acquisition )
```
!!!note
    It should be possible to disable the calculation of all counters if overhead needs to be reduced to a minimum

#### Data

`data` events will provide (data, metadata, detcnts, \*\*flags)
each event could contain one or several "acquisition points" for all active elements 

`data`:  3dim numpy array with `dims=(pointno, elementno, channel)`

`metadata`: 3dim numpy array with `dims=(pointno, elementno, metadata_idx)`

`detcnts`:  3dim numpy array with `dims=(pointno, elementno, cnt_idx)`

`flags`: dictionary with other information like `completed`, or other (`pixel info` for example).

#### Counter value production
On reception of a `data` event ROI values will be calculated at once for several points with numpy 
then published.

    
#### Standalone run (aka mcatake / mcaacq )
This is equivalent to a single point `loopscan` where only 1D detectors are selected. It could be 
done with a `ct` command selecting the 1D detectors.
GATE standalone will require the gate device to be included
But... data publishing in "refreshing" mode will be needed. 
This feature is currently missing in _bliss_.
A *polling_time* parameter deciding how often the update is done,  will be needed somewhere in the system>

#### Python API
Separation of controller and object
(to be developed: object available to users should provide the detector interface, not the bliss interface)




## The user view: User interface to 1D detectors 
1D detectors are integrated in _bliss_ as any other counter. They can be included in a MeasurementGroup
and hence acquired during a scan. 

#### Configuration
Configuration of the detector is done in beacon
ROI counters, and total sum counters must also be defined in beacon

#### Enable / Disable
standard bliss feature

#### Counter selection
standard bliss feature

#### Calibration handling

#### Standalone run

The user could run standalone runs to check the response of the detector. 

```sh
mcatake [exp-time] [save-yesno]
mcasave
```

## The programmmer view:  BLISS 1D Programmer API

The following is a draft proposal for what a new controller should implement to be integrated
in the general _bliss_ acquisition framework.

The aim is to facilitate the integration of new devices offering a clean, simple API for a variety
of developers, not necessarily core bliss developers.

The design of the API should also make sure that new 1D controllers can be added without
requiring changes in other _bliss_ modules. 

#### Producing 1D data / 0D counters / spectrum metadata
A 1D detector controller

#### Comments on properties 

!!!question
    the use of properties provide an elegant mechanism to include logic in 
    the access to class instance values. There is a small performance penalty of using properties 
    for accessing simple variables in objects but that could be considered negligible in most cases.
    
    The choice of using properties in different places in the core of _bliss_ might be adecuate as a
    software design principle. 
    
    On the other hand forcing the use of properties in the design of the 1D controller API may be more 
    controversial as it adds unnecessary complexity of the API definition and could be a source of errors 
    if not used properly, while the functionality bonus is unclear. 
    
    For those reasons, the API proposed below uses a more traditional functional scheme.

#### Basic controller API

*detector info*

```python
get_detector_brand(self) # returns a string with the brand/manufacturer info
get_detector_model(self) # returns a string with detector model details
```

*initialization*
```python
initialize_device(self)
```

*information*
```python
get_spectrum_size(self)
set_spectrum_size(self, spectrum_size)  # optional
```

*acquisition*
```python
get_preset_value(self)
set_preset_value(self,value)
start_acquisition(self)
stop_acquisition(self)
is_acquiring(self)
```


*data production* (finalize interface here - one or all of these)
```python
read_data(self)
   returns data, metadata, detcnts, flags 

update_read_data(self)
event.emit("data", (data, metadata, detctns, ** flags))
```

*if SYNC or GATE trigger modes are supported*
```python
supported_trigger_modes(self) # default:[SOFTWARE,]
# valid trigger modes:  SOFTWARE, SYNC, GATE
get_trigger_mode(self)  # default:SOFTWARE 
set_trigger_mode(self)

get_number_points(self)
set_number_points(self, points)
```

#### Additional methods

```python
finalize(self)
get_spectrum_range(self)  # default: [0,spectrum_size-1]
set_spectrum_range(self, range) 
get_number_elements(self) # default:1
get_element_geometry(self) # default:None
supported_preset_modes(self) # default:[REALTIME,]
# valid preset modes:  NONE, REALTIME, LIVETIME 
get_preset_mode(self) # default:REALTIME
set_preset_mode(self, preset_mode) 
```

## Metadata API
```python
get_metadata_description(self) # default:empty list or None
```

## Counter API
```python
get_counter_description(self) # default:empty list or None
```


## NOTES on specific controllers

*XIA Family*
```python
get_block_size(self)
set_block_size(self)
```

*XIA:Mercury*
```python
set_hw_sca_mode(self, flag) # True or False
set_hw_scas(self, scadefs)
get_hw_scas(self)
```

*XIA:FalconX*
```python
get_refresh_rate(self)
set_refresh_rate(self, refresh_rate)
```

*Mythen*

*MUSST MCA*

*PEPU* (not really - but an idea of acquiring something else)


## Missing general features in _bliss_

1D detectors would profit of a data updating mechanism that does not exist currently in _bliss_. 
This would allow for the implementation of macros like `mcatake` with automatic data refreshing.

That would also be of great interest for acquisition of 1D data during scans. Indeed, for long
acquisition times, users could have an updated display as data is being built. 

- update data mechanism

  It is not possible for now to *update* data in _redis_. Only new data can be added.
  Having this possibility as a general feature may provide new, interesting features like `uct` 
  macro or updated display of motor movements during scans.

  Modifications to data event handling in _bliss_ may be also needed. 

- This would imply modifications in the data writer mechanisms. For example data would need to be flagged
  as *completed* for the writer to consider actual file writing.

- _flint_ may require changes to support those changes


## On performance - and _LiMAc_ library
(*Li*brary for *M*CA *Ac*quisition)

Another different approach that may be needed to be considered for performance reasons 
could be the development of a Lima-like library for 1D controller plugin implementation. 

If such a library is produced, spectra should not travel to _bliss_ at all. 

That would have considerable development implications at several levels:

- data saving will be taken care of by the library
- data saving in _bliss_ will be done by *reference* to file
- _flint_ will display data online from generated data files (temporary files in the case of `mcatake`)
- ROI calculation will be done by library (or by hardware if the feature exists)
- Calibration should be provided from _bliss_ to the library for ROI calculation
- Counter values provided from library directly to _bliss_ 

The main advantage of such approach would be to be able to save data as it is produced 
as close as possible to the source. 

This approach would also generate less data traffic (no spectra being transferred) while at 
the same time less data storage requirements in _redis_.

## Reference: current files in _bliss_

```sh
bliss/controllers/mca/base.py
bliss/controllers/mca/counter.py
bliss/controllers/mca/roi.py
bliss/controllers/mca/xia.py
bliss/controllers/mca/mythen/*
bliss/scanning/acquisition/mca
```

