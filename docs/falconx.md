
---------------------------------------------------------
MOSCA Installation
---------------------------------------------------------

conda create -n mosca psutil gevent pytango 

git clone http://gitlab.esrf.fr/bcu/mosca/mosca.git

cd mosca
git submodule init
git submodule update
pip install -e .

# to run Hamamatsu spectrometers
pip install pythonnet

# to run OceanOptics spectrometers
pip install seabreeze

# run server
HamaSpectro.exe id16

# To create a "bat" file and link to it from a desktop icon. check and modify the file "mosca_bat_example.bat"

---------------------------------------------------------
Installation of pyhandel and Tango server for FalconX
---------------------------------------------------------

Install miniconda on Windows target
  (install for ALL users)

(recommended:  install "Microsoft Studio Code" if you plan to make changes )

Open miniconda powershell

Then:
::
   mkdir c:\blissadm
   cd c:\blissadm

   mkdir falconx
   mkdir falconx_config

falconx   contains official XIA software:  sitoro library, Prospect program
   
Install Prospect by clicking on the Prospect installer

mkdir falconx_config
   place config files in this 

git clone https://<user>@gitlab.esrf.fr/BCU/pyfalconx.git ./falconx_tango

conda config --env --set channel_priority false
conda config --env --add channels conda-forge
conda config --env --append channels defaults
conda config --env --append channels esrf-bcu
conda config --env --append channels tango-controls

conda create -n bliss python=3.7 bliss pytango git
conda activate bliss

cd falconx_tango

# test pyhandel
conda install matplotlib

test: 
   python test_pyhandel.py


# run Tango server from command line
set TANGO_HOST environment variable 
   open windows settings
   search "environment" -> "Edit environment variables for your account"

open anaconda powershell

# run Tango server
Copy file falconxtango.ps1-template to falconxtango.ps1

(edit falconxtango.ps1 with Notepad or other (microsoft studio code recommended)) 
   Set values for:
      TANGO_HOST

   eventually for BEACON_HOST (but has no effect)

   modify line:
      python FalconX.py  <instance-name>
    
   check values for:
      conda environment
      handel-sitoro dll location

   copy falconx-start.lnk to Desktop # link to start server
   # check eventually that properties (in particular the  execution path) of this link are correct 

   prepare instance in jive
      set properties for device
          mandatory:
           config_dir    c:\\blissadm\\falconx_config  # for example
           lib_dir       c:\\blissadm\\falconx\\handel-sitoro-fxn-1.1.22-x64\\lib  # for example

      other possible properties:
           config_file  <your favourite config file>  # default config.ini , files must be contained in lib_dir defined above
           lib_name    handel.dll  # default is handel.dll.  dll file must be in lib_dir directory defined abover

      following are possible but ignored for now:
           gate_ignore  True
           gate_master  0
           sync_master  0
           input_polairty  1
