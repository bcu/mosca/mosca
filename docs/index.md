
The MoSCa project, Module for Spectra Capture,
is a project developed at the ESRF with the goal of facilitating
the integration of 1D detectors in synchrotron beamline or accelerator 
control system. In principle the module could show useful if use in 
any other programming or experimental context.

MOSCA is fully written in python. The set of python dependencies has been
intentionally kept to a minimum to ease the use of the module in various
contexts.

If you are a programmer and you are using this module for the first time 
we strongly suggest you to start by getting familiar with the concepts 
and the structure of the project with the document: 
  [MoSCa Project Introduction for Integrators and Programmers](
    programmer_intro.md).

If you are using MoSCa from a control system for the first time
you may have a general feature description and a number of 
control system specific links in the document:
  [MoSCa Introduction for Control System Users ](
    user_intro.md).
    
## Table Of Contents

The MoSCa documentation consists of four separate document groups:
and consists of four separate parts:

1. [Tutorials](tutorial.md)
2. [How-To Guides](how-to-guides.md)
3. [Reference](reference.md)
4. [Explanation](explanation.md)

Quickly find what you're looking for depending on
your use case by looking at the different pages.

## Acknowledgements

I want to thank my house plants for providing me with
a negligible amount of oxygen each day. Also, I want
to thank the sun for providing more than half of their
nourishment free of charge.

