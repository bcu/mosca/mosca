
---------------------------------------------------------
MOSCA Installation
---------------------------------------------------------



conda create -n mosca psutil gevent pytango 

git clone http://gitlab.esrf.fr/bcu/mosca/mosca.git

cd mosca
git submodule init
git submodule update
pip install -e .

# to run Hamamatsu spectrometers
pip install pythonnet

# to run OceanOptics spectrometers
pip install seabreeze


HamaSpectro.exe id16

To create a "bat" file and link to it from a desktop icon. check and modify the file:

   mosca_bat_example.bat


Some of the devices may need extra installation steps. Check the corresponding pages.

# Extra installation options
#
#   C
