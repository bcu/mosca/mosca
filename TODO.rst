
BACKGROUND handling:
    - background and data saving during normal scans
     // DONE
   
Saving data
   - save option to save both (corrected and raw) data
     // attribute "saving_choice" - DONE

calibration
   - macro to show/load/enter calib params - DONE
   // macro:  moscacalib

messages after each zap with saving info (after each zapline (including during zapimage) 
    # DONE to be tested

message at the beginning of zap with background info / preset time info 
    # DONE to be tested

Wrong numbering of files during zap  - DONE

Error with mca_spar() not configured for device 1 - DONE
   # related with mcaoff in setup

mosca menu

   options
    - ct/scans

    - zap use

    - background  (during scan / during zap)
            * use correction
            * take a new one

    - saving prefix / directory / choice 
            * for step by step


OTHER:
   Include data_info in headers 
   Resend rois (detect difference) after restarting server


# PENDING - ISSUES to fix

## General

calculate_counters() revise.  it seems sometimes one less counter is calculated (at the beginning?)
   NOTE: for that the problem is hidden as all counters are recalculated every time

##  Hamamatsu

Protect programming 
   do not program too small or too large preset_value

Why the first point after each start is lower (?)
   test to add a sleep time after start and compare

Conversion of folder to windows path

Work on calibration
   MCA_DATA1_PARAM


#
#  TO TEST
#
new attributes with info about background - DONE
   - data/time of background acquisition (in memory)
   - exposure/time of background acquisition 

# FIXED issues - features 
Protect programming 
   only one "stop" at a time - DONE

create new general attribute/property -  "detector_name" (to be included in saving filename) - DONE

readout_time attribute   # make sure is RW and coherent between devices - DONE

if bkg is active but not taking a background still save the background at each zapscan - DONE
(implement saveBackground) - DONE
separate in "mosca" menu between active for CT and active for ZAP - DONE

moscabkg off   - DONE
  still saving a file during zap

