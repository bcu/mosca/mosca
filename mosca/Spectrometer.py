"""
    File:  Spectrometer.py

    Description: 
       Base support class for Spectrometer classes
           SpectroDataHandler()

       It provides features for:
          - data handling
          - automatic counter calculation
          - data saving
          - background data correction 

       The logic includes support for multi-channel spectrometer
     
       This class is a Singleton, i.e. calls to create a
       SpectroDataHandler() within one program will always return
       the same object instance. 

   Check the documentation in the docs/ directory for details about how to
   use this class.
"""

from enum import Enum
import numpy as np
import os
import re
import copy
import psutil
import math
import datetime
import time

import threading
import gevent
from collections import OrderedDict

DEBUG_ACCUM = 0

try:
    import fabio

    fabio_ok = True
except ImportError:
    fabio_ok = False

class PresetMode(Enum):
    NONE = 0x01
    REALTIME = 0x02
    LIVETIME = 0x04
    EVENTS = 0x08
    TRIGGERS = 0x10

class TriggerMode(Enum):
    SOFTWARE = 0x01
    SYNC = 0x02  # SYNC = TRIG
    GATE = 0x04

class AcquisitionMode(Enum):
    SINGLE = 0x01
    CONCATENATION = 0x02
    ACCUMULATION = 0x04

class Saving(Enum):
    MANUAL = 0x01
    AUTO = 0x02

class SavingChoice(Enum):
    RAW = 0x01
    CORRECTED = 0x02
    BOTH = 0x04

class SavingOverwritePolicy(Enum):
    OVERWRITE = 0x10
    ABORT = 0x20
    SAVEALL = 0x40

class SavingFormat(Enum):
    HDF5 = 0x01
    EDF = 0x02
    EDF_XMAP = 0x04
    REDIS = 0x10

class MemoryManagement(Enum):
    NONE = 0x01
    MAX_MEMORY = 0x02
    ON_READ = 0x04
    ON_DEMAND = 0x08

class Singleton(object):
    __instance = None

    def __new__(cls, *args, **kwargs):
        print_sdh("Singleton called")
        if cls.__instance is None:
            cls.__instance = super(Singleton, cls).__new__(cls, *args, **kwargs)
        return cls.__instance


class Counter():
    def __init__(self, cntname, chanrange, 
                   detector=None, 
                   correction=0, 
                   number=-1, 
                   multichannel=False):

        self.name = cntname
        self.number = number
        print(f' Adding counter - chanrange={chanrange} type={type(chanrange)}')
        self.fch, self.lch = map(int,chanrange)
        self.correction = correction
        self.multichannel = multichannel

        print(f'defining detector {cntname} on det={detector} fpx={self.fch} lpx={self.lch}')

        if self.multichannel:
            if detector is None:
                raise BaseException("wrong counter definition. multichannel needs detector specification") 
            self.set_detector(detector)

    def set_number(self, number):
        self.number = number

    def get_number(self):
        return self.number 

    def get_range(self):
        return self.fch, self.lch

    def get_detector(self):
        return self.fdet, self.ldet

    def set_detector(self, detector):
        if isinstance(detector,str) and detector.find("-") > 0:
            self.fdet, self.ldet = map(int, detector.split("-"))
        else:
            if detector == "all" or int(detector) == -1:
                self.fdet = "all"
                self.ldet = None
            else:
                self.fdet = int(detector)
                self.ldet = None

    def get_info(self,as_num=False):
        if self.multichannel:
            if self.ldet is not None:
                detinfo = f"{self.fdet}-{self.ldet}"
            else:
                detinfo = self.fdet

        if as_num:
            if self.multichannel:
                return [detinfo, self.fch, self.lch, self.correction]
            else:
                return [self.fch, self.lch, self.correction]
        else:
            if self.multichannel:
                return f"name={self.name} det={detinfo} chans={self.fch}:{self.lch} corr={self.correction}"
            else:
                return f"name={self.name} chans={self.fch}:{self.lch} corr={self.correction}"

class StoredData:
    def __init__(self, data, data_info=None):
        self.data = data
        if data_info is None:
            self.data_info = {}
        else:
            self.data_info = copy.copy(data_info)

    @property  # datetime 
    def time(self):
        return self.data_info.get('time', None)

    @time.setter 
    def time(self, value):
        self.data_info['time'] = value

    @property  # exposure time in ms
    def exptime(self):
        return self.data_info.get('preset_value', None)

    @exptime.setter
    def exptime(self, value):
        self.data_info['preset_value'] = value


def print_sdh(msg):
    print(f"  [DataHandler] {msg}")


class SpectroDataHandler(Singleton):

    _initialized = False

    def __init__(self):
        print_sdh("__init__()")
        if not self._initialized:
            self.set_defaults()
            self.init_counters()
            self.init_data()
            self.init_store()
            self.initialized = True

    @property
    def multichannel(self):
        return self.is_multichannel 

    @multichannel.setter
    def multichannel(self,value):
        self.is_multichannel = value

    def set_number_channels(self, nb_chan):
        self.nb_det_chans = nb_chan

    def set_ignore_first(self, ignore):
        # ignore first flag
        #    apply for now only to saving
        #    should it also be applied for ROI counters?
        self.ignore_first = ignore

    def get_ignore_first(self):
        return self.ignore_first 

    def set_defaults(self):
        self.acq_mode = AcquisitionMode.SINGLE
        self.accum_nb_frames = 1

        self.spectrum_size = 512
        self.nb_det_chans = 1
        self.is_multichannel = False

        self.ignore_first = 0

        self.metadata_labels = []
        self.metadata_size = 0

        # variables affecting memory allocation and usage
        #  by default max_vmem is set here to 0.63 (63%) of total PC memory
        #  the limiting factor is the minimum of max_vmem and the memory 
        #  associated to with the buffer_max_pixels
        self.max_vmem = 0.30
        self.buffer_max_pixels = 250000

        # the following two lines de-activate "resizing"
        self.buffer_pixel_chunk = self.buffer_max_pixels
        self.allow_resize = False  

        self.detector_name = ""

        self.update_pixel_size()

        self.saving_root = "c:\\blissadm\\test_data"
        self.saving_format = SavingFormat.EDF_XMAP
        self.saving_mode = Saving.MANUAL
        self.saving_mode_inuse = Saving.MANUAL
        self.saving_choice = SavingChoice.RAW
        self.saving_directory = "data"
        self.saving_prefix = "spectro"
        self.saving_suffix = ".edf"
        self.saving_reverse = False
        self.saving_auto_reverse = False
        self.saving_index_format = "%04d"
        self.saving_number_set = False

        self.saving_sumspec = "all"
        self.saving_sums = "all"

        self.using_xmap_numbers = False

        self.headers = {}
        self.header_keep = [] # list of header keys that are not affected
                              # by "reset_headers"

        self.saving_points_per_file = 10000
        self.saving_number = 0
        self.saving_overwrite_policy = SavingOverwritePolicy.ABORT
        self.saving_xmap_numbers = []
        self.saved_filename = ""
        self.save_starting = False
        self._saving_threads = []

        self.data = np.zeros((1,1))
        self.metadata = np.zeros((1,1), dtype=float)

    def init_counters(self):
        self.nb_counters = 0
        self.counters = OrderedDict()
        self.counter_values = {}

    def init_data(self, spectrum_size=1024, nb_chans=None, data_info=None):
        self.no_pixels = 0
        self.no_meta_pixels = 0
        self.last_calc_px = 0
        self.calc_all = True

        if data_info is None:
            self.data_info = {}
        else:
            self.data_info = data_info

        if not 'time' in self.data_info: 
            self.data_info['time'] = datetime.datetime.now()

        self.data_info['timestamp'] = time.perf_counter()

        # initial memory size is dimensioned for 2048 spectra
        # then, if needed new space will be added with that
        # chunk size.  TODO. make the 2048 configurable

        if nb_chans is not None:
            self.nb_det_chans = nb_chans

        if self.nb_det_chans > 1:
            self.multichannel = True

        self.spectrum_size = spectrum_size

        # np.int32 is 4 bytes
        self.update_pixel_size()

        print_sdh(f"Init nb det chans={self.nb_det_chans}, spectrum size={spectrum_size}"
                  f" ,  max pixels={self.spectra_buffer_size}")

        self.current_buffer_size = 0
        self.buffer_first_pixel = 0

        self.init_counter_values()

        self.accumulated = 0
        self.accum_missing = 0

        self.init_data_saving()
        self.rearm_saving('initacq')

    def init_store(self):
        # store is an area where named 
        # data objects can be saved to be used 
        # during processing:
        #    background or other
        self.store = {}
        self.background_correction = False
        self.background_stored = False

    def buffer_check_add(self, npixels):
        #
        # TODO. check that npixels is not greater than buffer_pixel_chunk
        #     and not bigger than spectra_buffer_size
        #
        nb_spectra_total = self.no_pixels + npixels

        # space is enough. do nothing
        if self.current_buffer_size >= nb_spectra_total:
             # ok. enough space for new spectra. no need to grow, no need to roll")
             return

        # not enough space but we can still grow.  add a chunk
        if self.current_buffer_size < self.spectra_buffer_size: 
            if self.allow_resize or self.current_buffer_size == 0:
                self.alloc_spectra_buffer()

        # if after growing all the spectra fit in buffer. 
        # then we are good. we do not need to roll out
        if self.current_buffer_size >= nb_spectra_total: 
            return

        # how much extra space we need?
        #  now showing self.buffer_first_pixel to self.
        fpx = self.buffer_first_pixel
        lpx = fpx + self.spectra_buffer_size

        # rolling 
        nb_roll = nb_spectra_total - lpx
        
        if self.calc_needed:
            self.calculate_counters()

        if nb_roll > 0:
            self.rollit(nb_roll)
            #self.rolldata(nb_roll)

    def alloc_spectra_buffer(self):
        meta_size = self.metadata_size
        spect_size = self.spectrum_size

        if self.allow_resize:
            new_size = min(self.current_buffer_size + self.buffer_pixel_chunk, 
                           self.spectra_buffer_size)
        else:
            new_size = self.spectra_buffer_size
            
        to_add = new_size - self.current_buffer_size    

        if self.multichannel:
            zeroes = np.zeros((self.nb_det_chans, to_add, spect_size), dtype=np.uint32)
            if meta_size > 0:
                mzeroes = np.zeros((self.nb_det_chans, to_add, meta_size), dtype=float)

            if self.current_buffer_size == 0:
                self.data = zeroes
                if meta_size > 0:
                    self.metadata = mzeroes
            else:
                self.data = np.hstack((self.data,zeroes))
                if meta_size > 0:
                    self.metadata = np.hstack((self.metadata,mzeroes))
        else:
            zeroes = np.zeros((to_add, spect_size), dtype=np.uint32)
            if meta_size > 0:
                mzeroes = np.zeros((to_add, meta_size), dtype=float)
    
            if self.current_buffer_size == 0:
                self.data = zeroes
                if meta_size > 0:
                    self.metadata = mzeroes
            else:
                self.data = np.vstack((self.data,zeroes))
                if meta_size > 0:
                    self.metadata = np.vstack((self.metadata,mzeroes))

        self.current_buffer_size = new_size

    def rollit(self, nb_roll):
        if self.multichannel:
            axis = 1
        else:
            axis = 0

        self.data = np.roll(self.data, -nb_roll, axis=axis)
        self.metadata = np.roll(self.metadata, -nb_roll, axis=axis)

        self.buffer_first_pixel += nb_roll
        print(f"rolling the data buffers by {nb_roll} pixels. the first pixel in buffer is now {self.buffer_first_pixel}")

    def rolldata(self, nb_roll):
        # this is slightly slower. no advantage
        if self.multichannel:
            m, n, l = self.data.shape
            indices = np.arange(nb_roll,n)
            self.data[:,0:n-nb_roll,:] = np.take(self.data, indices, axis=1)
            self.metadata[:,0:n-nb_roll,:] = np.take(self.metadata, indices, axis=1)
        else:
            m, n = self.data.shape
            indices = np.arange(nb_roll,m)
            self.data[0:m-nb_roll,:] = np.take(self.data, indices, axis=0)
            self.metadata[0:m-nb_roll,:] = np.take(self.metadata, indices, axis=0)

        self.buffer_first_pixel += nb_roll
        print(f"rolling the data buffers by {nb_roll} pixels. the first pixel in buffer is now {self.buffer_first_pixel}")

    def update_max_vmem_usage(self, maxvmem=None):
        """ value is in percentage of computer virtual memory size """
        if maxvmem is not None:
            if maxvmem < 0.01 or maxvmem > 0.8:
                raise BaseException("pymosca error. maximum virtual memory should be in the range 0.01 to 0.8") 

            self.max_vmem = maxvmem

        self.max_nb_bytes = psutil.virtual_memory().total * self.max_vmem
        self.max_mem_pixels = math.floor( self.max_nb_bytes / self._pixel_size )

    """ spectra buffer size 
        number of spectra that can be hold at any time in the spectra ring buffer
    """
    def update_pixel_size(self):
        self._pixel_size = self.nb_det_chans * (self.spectrum_size + self.metadata_size) * 4
        self.update_max_vmem_usage()
        self.update_max_buffer_size()

    def update_max_buffer_size(self):
        self._buffer_max_pixels = min(self.buffer_max_pixels, self.max_mem_pixels)

    @property
    def spectra_buffer_size(self):
        return self._buffer_max_pixels
    
    @spectra_buffer_size.setter
    def spectra_buffer_size(self, value):
        self.buffer_max_pixels = value
        self.update_max_buffer_size()

    @property
    def buffer_pixel_chunk(self):
        return self._buffer_pixel_chunk 
    
    @buffer_pixel_chunk.setter
    def buffer_pixel_chunk(self, value):
        self._buffer_pixel_chunk = value

    def set_detector_name(self, name):
        self.detector_name = name

    def init_data_saving(self):
        #if not self.is_saving_done():
            #print(f'DATA saving in progrress. WAIT...')
            #self.wait_save()
            #print(f'  - ok. completed.')

        #if not self.using_xmap_numbers and not self.saving_number_set:
            #self.saving_number = 0

        self.save_starting = False
        self.saving_thread = None

        # to avoid change of behaviour while acquiring
        self.saving_mode_inuse = self.saving_mode

        self.saved_nb_files = 0
        self._saving_threads = []

    def init_counter_values(self):
        for cntname in self.counters:
            self.counter_values[cntname] = []
        self.calc_needed = True

    def get_acquisition_mode(self):
        return self.acq_mode

    def set_acquisition_mode(self, mode):
        self.acq_mode = mode

    def get_accum_nb_frames(self):
        return self.accum_nb_frames

    # sets the number of frames adding up for an accumulated frame
    def set_accum_nb_frames(self, value):
        self.accum_nb_frames = value

    def correct_background_on(self):
        self.background_correction = True
        if self.background_stored:
            self.calc_needed = True
            self.calc_all = True

    def correct_background_off(self):
        self.background_correction = False
        if self.background_stored:
            self.calc_needed = True
            self.calc_all = True

    def is_background_correction_on(self):
        # print(f'bckg. activ {self.background_correction} data stored {self.background_stored}')
        return (self.background_stored and self.background_correction)

    def save_background(self):
        if self.background_stored is False:
            print(f'No background to save')
            return

        bckg = self.store.get("background")
        data = bckg.data
        saving_number = 0
        reverse = False
        headers =  bckg.data_info
        metadata = None

        self._do_save(data, metadata, headers, saving_number, reverse, data_type="bkg")

    def store_background(self,data=None):

        # if no data is provided the latest pixel in memory is kept
        if data is not None: 
            if not isinstance(data, np.ndarray): 
                # try creating an array. this makes a copy at the same t
                # we cannot really check the shape of data at this point
                # just keep it
                data = np.array(data)
        elif data is None and self.no_pixels:  
            pxno = self.no_pixels - self.buffer_first_pixel - 1
            sp_size = self.spectrum_size
            if self.multichannel:
                nct = self.nb_det_chans
                data = self.data[:,pxno].reshape([nct,1,sp_size])
            else:
                data = self.data[pxno].reshape([1,sp_size])

        # this will create a copy of the original data 
        bckg_data = copy.copy(data)
        self.store_data(bckg_data, 'background', data_info=self.data_info)
        self.background_stored = True

    def store_data(self, data, name=None, data_info=None):
        # to store any data with a name in the "store"
        #   
        to_store = StoredData(data, data_info)
        self.store[name] = to_store
    #
    # Receives data with new spectra
    #   for 1 channel:
    #      1d numpy array with one spectrum
    #          spectra[...]
    #      2d numpy array with series of pixels. 
    #          ( first dimension = pxno, Second dimension: counts )
    #          spectra[pxno][...]
    #
    #   for multichannel:
    #      2d numpy array with data for one pixel for all channels
    #         spectra[chno][...] 
    #      3d numpy array with data for several pixels for all channels
    #         spectra[chno][pxno][...]
    #
    #
    def add_spectra(self, spectra, metadata=None, completed=True):
        dims = spectra.shape

        if completed == False:
            #  what to do with temporary frames?
            return 

        # we should check that provided spectra correspond to spectrum_size and nb_det_chans expected

        if self.multichannel:
            nchans = dims[0]

            if len(dims) == 3:
                npixels = dims[1]
                nvals = dims[2] 
                if metadata is not None:
                    meta_nvals = metadata.shape[2]
                    metadata = metadata.reshape((nchans, npixels, meta_nvals))
            elif len(dims) == 2:
                # one pixel for all channels
                npixels = 1
                nvals = dims[1] 
                spectra = spectra.reshape((nchans, npixels, nvals))
                if metadata is not None:
                    meta_nvals = metadata.shape[1]
                    metadata = metadata.reshape((nchans, npixels, meta_nvals))
            else:
                raise BaseException("Wrong array dimensions provided to add_spectra (multichannel)")
        else:
            if len(dims) == 2:
                npixels, nvals = dims
                if metadata is not None:
                    meta_nvals = metadata.shape[1]
                    metadata = metadata.reshape((npixels, meta_nvals))
            elif len(dims) == 1:
                npixels = 1
                nvals = dims[0]
                spectra = spectra.reshape((1, nvals))
                if metadata is not None:
                    meta_nvals = metadata.shape[0]
                    metadata = metadata.reshape((1, meta_nvals))
            else:
                raise BaseException("Wrong array dimensions provided to add_spectra")

        if nvals != self.spectrum_size:
            raise BaseException(f"Wrong spectrum size provided to add_spectra {nvals}. expected is {self.spectrum_size}")
         
        if self.multichannel:
            if nchans != self.nb_det_chans:
                raise BaseException(f"Wrong number of channels provided to add_spectra {nchans}. expected is {self.nb_det_chans}")

        if metadata is not None:
            if meta_nvals != self.metadata_size:
                raise BaseException(f"Wrong number of metadata values provided to add_spectra {meta_nvals}. expected is {self.metadata_size}")


        if self.acq_mode == AcquisitionMode.SINGLE:
            self.buffer_check_add(npixels)
            self._add_spectra_single(spectra, metadata, npixels)
        elif self.acq_mode == AcquisitionMode.ACCUMULATION:
            self._add_spectra_accum(spectra, metadata, npixels)

        if self.saving_mode_inuse == Saving.AUTO:
            self.save_starting = True
            self.check_save()
            self.save_starting = False

        # print(f' - <DataHandler>. {self.no_pixels} spectra added so far. data is {self.data.shape} - metadata is {self.metadata.shape}')

        self.calc_needed = True

    def _add_spectra_single(self, spectra, metadata, npixel):

        fpx = self.no_pixels - self.buffer_first_pixel
        lpx = fpx + npixel

        if self.multichannel:
            self.data[:,fpx:lpx] = spectra
            if metadata is not None:
                self.metadata[:,fpx:lpx,:] = metadata[:,:,:]
        else:
            self.data[fpx:lpx:] = spectra
            if metadata is not None:
                self.metadata[fpx:lpx:] = metadata

        self.no_pixels += npixel

    def _add_spectra_accum(self, spectra, metadata, npixels):
        nb_acc_frames = self.accum_nb_frames

        frame_ptr = 0
        dims = spectra.shape

        remaining = received = npixels

        while remaining > 0:
            # how many pixels should added in this iteration
            if self.accum_missing:
                completing = True
                if remaining >= self.accum_missing:
                    adding = self.accum_missing
                else:
                    adding = remaining
            else:
                completing = False
                if remaining > self.accum_nb_frames:
                    adding = self.accum_nb_frames
                else:
                    adding = remaining

            if received == 1:
                accum_frame = spectra
            else:
                if self.multichannel:
                    # add on second index (pxno) 
                    # make sure the accum_frame respects [chno, 1, spectrum_size]
                    accum_frame = np.sum(spectra[:,frame_ptr : frame_ptr + adding], axis=1).reshape((dims[0],1,dims[2]))
                else:
                    accum_frame = np.sum(spectra[frame_ptr : frame_ptr + adding], axis=0).reshape(1,dims[1])

            pxno = self.no_pixels - self.buffer_first_pixel
            if completing:
                # fill latest frame
                if self.multichannel:
                    self.data[:,pxno-1,:] = self.data[:,pxno-1,:] + accum_frame
                else:
                    self.data[pxno-1,:] = self.data[pxno-1,:] + accum_frame
            else:
                # adding new frame
                self.buffer_check_add(1)
                if self.multichannel:
                    self.data[:,pxno,:] = accum_frame
                else:
                    self.data[pxno,:] = accum_frame
                self.no_pixels += 1

            frame_ptr += adding
            self.accumulated += adding
            remaining -= adding

            self.accum_missing = int(self.accumulated % self.accum_nb_frames)
            if self.accum_missing != 0:
                self.accum_missing = int(self.accum_nb_frames - self.accum_missing)

    def get_number_pixels(self):
        return(self.no_pixels)

    def data_completed(self):

        if self.saving_mode_inuse == Saving.AUTO:
            self.save_starting = True
            self.check_save(saveall=True)
            # self.wait_save()
            self.save_starting = False

        if self.acq_mode == AcquisitionMode.ACCUMULATION and self.accum_missing:
            print("Warning - last frame in accumulation mode is not complete")

        self.last_acq_time = datetime.datetime.now()
        self.calc_needed = True

    def get_spectra(self, from_px=0, to_px=-1):

        if to_px == -1:
            to_px = self.no_pixels-1

        nb_px = to_px-from_px

        fpx = from_px - self.buffer_first_pixel 
        lpx = from_px + to_px

        sp_size = self.spectrum_size

        if self.multichannel:
            nct = self.nb_det_chans
            data = self.data[:,fpx:lpx+1].reshape(nct,nb_px,sp_size)
        else:
            data = self.data[fpx:lpx+1].reshape(nb_px,sp_size)

        return data

    def get_background_data(self,flatten=False):
        bckg_data = self.store.get("background", None)

        if bckg_data is None:
            return None

        if flatten:
            return bckg_data.flatten()
        else:
            return bckg_data

    def get_data(self, from_px=0, to_px=-1, flatten=False, corrected=True):

        if self.no_pixels == 0:
            data = self.empty_data()
        else:
            if self.multichannel:
                data = self.data[:,:self.no_pixels,:]
            else:
                data = self.data[:self.no_pixels,:]

        # background correction
        if self.is_background_correction_on() and corrected:
            bckg = self.store.get("background")
            bckg_data = bckg.data
            if self.acq_mode == AcquisitionMode.ACCUMULATION:
                print(f'substracting bkg multiplied by {self.accum_nb_frames}')
                bckg_data = bckg_data * self.accum_nb_frames
            data = data - bckg_data.data
            data[data<0] = 0

        if from_px < 0:
            from_px = self.no_pixels + from_px

        if to_px < 0:
            to_px = self.no_pixels + to_px

        if from_px < self.buffer_first_pixel:
            fpx = 0
        else:
            fpx = from_px - self.buffer_first_pixel 

        if to_px < self.buffer_first_pixel:
            lpx = 0
        else:
            lpx = to_px - self.buffer_first_pixel 

        if self.multichannel:
            d = data[:,fpx:lpx+1,:]
        else:
            d = data[fpx:lpx+1,:]

        if flatten:
            return d.flatten()
        else:
            return d

    def empty_data(self):
        if self.multichannel:
            empty = np.zeros(self.nb_det_chans*self.spectrum_size)
            empty.shape = self.nb_det_chans,1,self.spectrum_size
        else:
            empty = np.zeros(self.spectrum_size)
            empty.shape = 1,self.spectrum_size
        return empty

    def get_metadata(self, from_px=0, to_px=-1, orderby='det',flatten=False):
        if to_px == -1:
            to_px = self.no_pixels-1

        if from_px < self.buffer_first_pixel:
            fpx = 0
        else:
            fpx = from_px - self.buffer_first_pixel 

        if to_px < self.buffer_first_pixel:
            lpx = 0
        else:
            lpx = to_px - self.buffer_first_pixel 


        if self.multichannel:
            md = self.metadata[:,fpx:lpx+1,:]
        else:
            md = self.metadata[fpx:lpx+1,:]

        if flatten:
            if orderby == 'pixel' and self.multichannel:
                mdf = np.swapaxes(md,0,1)
                return mdf.flatten()
                # sz = md.shape[0] * md.shape[1] * md.shape[2]
                # mdf = np.zeros(sz, dtype=md.dtype)
                # nb_px = md.shape[1]
                # ptr = 0
                # ptr_step = md.shape[0] * md.shape[2]
                # for pxno in range(nb_px):
                #     d = md[:,pxno].flatten()
                #     mdf[ptr:ptr+ptr_step] = d
                #     ptr += ptr_step
                # return mdf
            else:
                return md.flatten()
        else:
            return md

    def set_metadata_labels(self, labels):
        """
        """
        if isinstance(labels, list):
            self.metadata_labels = labels
            self.metadata_size = len(labels)

    def get_metadata_labels(self):
        return self.metadata_labels

    def add_counter(self, *args):
        #  [detch_no] fch lch [corr]
        #
        # For multichannel 3 values are needed:
        #      chno = detchannel number (-1 would sum over all det channels)
        #      fch = counter first spectrum channel 
        #      lch = counter last spectrum channel 
        #
        # for single channel detector chno is not needed
        #      fch, lch
        #
        # extra parameters "corr" is ignored for now if present
        #   this was possible with old taco server. Still useful?
        #
        correction = 0
        cntname = args[0]
        vals = args[1:]
        if self.multichannel:
            detector = vals[0]
            if len(vals) > 1:
                chanrange = vals[1:3]
            else:
                chanrange = [1,-1]
            if len(vals) > 4:
                correction = vals[4]
            print(f'Spectromer adding counter {cntname} - det={detector} range={chanrange}')
        else:
            detector = None
            chanrange = vals[0:2]
            if len(vals) > 3:
                correction = vals[3]
            print(f'Spectromer adding counter {cntname} - range={chanrange}')


        counter = Counter(cntname, chanrange, 
                   detector=detector, correction=correction, 
                   multichannel=self.multichannel)

        if cntname not in self.counters:
            cntno = self.nb_counters
            counter.set_number(cntno)
            self.nb_counters += 1

        self.counters[cntname] = counter
        self.counter_values[cntname] = []

        self.calc_needed = True
        self.last_calc_px = 0

    def set_counter_detector(self, cntname, detector):
        if cntname not in self.counters:
            raise BaseException(f"counter with name {cntname} does not exist")
        cnt = self.counters[cntname]
        cnt.set_detector(detector)

    def calculate_counters(self):
        npx = self.no_pixels - self.last_calc_px

        print(f'calculating counters')
        self.calc_all = True

        vals = []
        cntname = ""

        if self.calc_all:
            self.last_calc_px = 0

        if self.last_calc_px == 0:
            self.calc_all = True

        if self.buffer_first_pixel > 0:
            # cannot recalculate all values if not all data is in memory
            self.calc_all = False
        
        if self.calc_all:
            fidx = 0
        else:
            fidx = self.last_calc_px

        if self.last_calc_px == 0:
            fidx = 0
            lidx = self.no_pixels - self.buffer_first_pixel
        elif self.last_calc_px < self.buffer_first_pixel:
            # we lost pixels to calculate. 
            # this could happen if counters are defined after the acquisition
            # and not all data in memory any more
            fidx = 0
            lidx = self.no_pixels - self.buffer_first_pixel
            print(f'WARNING: Counters cannot be calculated correctly. Some pixels are lost')
        else:
            npx = self.no_pixels - self.last_calc_px
            fidx = self.last_calc_px - self.buffer_first_pixel
            lidx = fidx + npx - 1

        data = self.get_data()

        for cntname, cnt in self.counters.items():
            fch, lch = cnt.get_range()
            if self.multichannel:
                fdet, ldet = cnt.get_detector()
                if ldet is None:
                    if fdet == "all":  
                        vals = np.sum(data[:,fidx:lidx+1,fch:lch+1], axis=(0,2))
                    else:
                        vals = np.sum(data[fdet, fidx:lidx+1, fch:lch+1], axis=1)
                else:
                    vals = np.sum(data[fdet:ldet+1, fidx:lidx+1, fch:lch+1], axis=(0,2))
            else:
                lidx = self.no_pixels - self.buffer_first_pixel
                vals = np.sum(data[fidx:lidx+1, fch:lch+1], axis=1)

            if self.calc_all:
                self.counter_values[cntname] = vals
            else:
                self.counter_values[cntname] = np.concatenate([self.counter_values[cntname], vals])
      
        self.calc_needed = False
        if cntname:
            print(f'CALCULATE counters - npixels={self.no_pixels} - nb_calc(prev)={self.last_calc_px}  - nb_new_vals={len(vals)} - total={len(self.counter_values[cntname])} ')
        else:
            print(f'CALCULATE counters - npixels={self.no_pixels} - nb_calc(prev)={self.last_calc_px}  - nb_new_vals={len(vals)} - total={len(self.counter_values[cntname])} ')
        self.last_calc_px = self.no_pixels 

    def get_counter_names(self):
        return [cntname for cntname in self.counters]

    def get_counter_values(self, cntname=None, frompx=0, topx=None, flatten="counter"):
        try:
            if self.calc_needed:
                print(f'Recalculating counters')
                self.calculate_counters()
    
            # only one counters is requested
            if cntname is not None:
                if cntname in self.counter_values:
                    return self.counter_values[cntname][frompx:topx]
                else:
                    return 0
    
            # if all counters are requested
            first = True
    
            arr_values = np.zeros(0)
    
            for cntname in self.counters:
                if first:
                    arr_values = np.array(self.counter_values[cntname][frompx:topx])
                    first = False
                else:
                    arr_values = np.vstack((arr_values, self.counter_values[cntname][frompx:topx]))

        except BaseException as e:
            import traceback
            traceback.print_exc()
            raise(e)

        if flatten:
            if flatten == 'pixel':
                arr_values = np.transpose(arr_values).flatten()
            else:
                arr_values = arr_values.flatten()
        
        return arr_values

    def get_counter_info(self, cntname, as_num=False):
        if cntname in self.counters:
            cnt = self.counters[cntname]
            return cnt.get_info(as_num)
        else:
            return "counter not defined"

    def get_all_counter_info(self, as_num=False):
        ctdefs = []
        for ctname, cnt in self.counters.items():
            if as_num: 
                ctdefs.extend(cnt.get_info(as_num=True))
            else:
                ctdefs.append(cnt.get_info())

        return list(map(str,ctdefs))

    def get_counter_value(self, cntname, frompx=0, topx=None):
        try:
            nb_vals = len(self.counter_values[cntname])
            print(f'get_counter_value for {cntname} - npixels={self.no_pixels} nb_vals={nb_vals}')
        except BaseException as e:
            print(f'error is {e}')

        if nb_vals < self.no_pixels:
            self.calc_needed = True

        if self.calc_needed:
            self.calculate_counters()

        if cntname in self.counters:
            if topx is None:
                return self.counter_values[cntname][frompx:]
            else:
                return self.counter_values[cntname][frompx:topx]
        else:
            raise BaseException(f'counter name not defined')

    #
    # SAVING
    #
    def get_saving_rootpath(self):
        return self.saving_root

    def set_saving_rootpath(self, path):
        abspath = os.path.abspath(path)
        if self.saving_root != abspath:
            self.saving_root = abspath
            self.rearm_saving('rootpath')

    def get_saving_directory(self):
        return self.saving_directory

    def set_saving_directory(self, value):
        if value != self.saving_directory:
            self.saving_directory = value
            self.rearm_saving('saving_directory')

    def get_prefix_and_name(self, data_type="data"):
        if self.detector_name:
            prefix = f"{self.saving_prefix}_{self.detector_name}"
        else:
            prefix = self.saving_prefix
        
        if data_type != 'data':
            prefix = f'{prefix}_{data_type}'

        return prefix

    def get_saving_prefix(self):
        return self.saving_prefix

    def set_saving_prefix(self, value):
        if value != self.saving_prefix:
            self.saving_prefix = value
            self.rearm_saving('saving_prefix')

    def get_saving_suffix(self):
        return self.saving_suffix

    def set_saving_suffix(self, value):
        if self.saving_suffix != value:
            self.saving_suffix = value
            self.rearm_saving('suffix')

    def get_saving_format(self):
        return self.saving_format

    def set_saving_format(self, save_format):
        if save_format in SavingFormat:
            self.saving_format = save_format
            if self.saving_format == SavingFormat.HDF5:
                self.set_saving_suffix(".h5")
            elif self.saving_format in (SavingFormat.EDF, SavingFormat.EDF_XMAP):
                self.set_saving_suffix(".edf")
        else:
            raise BaseException("unsupported saving file format")

    def get_saving_choice(self):
        return self.saving_choice

    def set_saving_choice(self, choice):
        if choice in SavingChoice:
            self.saving_choice = choice
        else:
            raise BaseException("unsupported saving file format")

    def get_saving_mode(self):
        return self.saving_mode

    def set_saving_mode(self, mode):
        if mode in [Saving.MANUAL, Saving.AUTO]:
            self.saving_mode = mode
        else:
            raise BaseException("unsupported saving mode")

    def set_saving_auto_reverse(self, reverse):
        if self.saving_auto_reverse != reverse:
            self.saving_auto_reverse = reverse
            self.rearm_saving('auto_reverse')

    def get_saving_auto_reverse(self):
        return self.saving_auto_reverse

    def set_saving_reverse(self, reverse):
        if self.saving_reverse != reverse:
            self.saving_reverse = reverse
            self.rearm_saving('reverse')

    def get_saving_reverse(self):
        return self.saving_reverse

    def get_saving_sumspec(self):
        return self.saving_sumspec

    def set_saving_sumspec(self, sumspec):
        if sumspec.lower() == "none":
            self.saving_sums = None
        elif sumspec.lower() == "all":
            self.saving_sums = "all"
        else:
            try:      
                self.saving_sums = self.sumspec_parse(sumspec)
            except BaseException as e:
                raise BaseException(f'Wrong sum specifiation: {str(e)}') 
        self.saving_sumspec = sumspec

    def sumspec_parse(self, sumspec):
        maxchans = self.nb_det_chans 
        _sums = []
        for s in sumspec.split(";"):
            onel = []
            for cval in s.split(","):
                mat = re.match("(?P<fch>\d+)\-(?P<lch>\d+)", cval)
                if mat:
                    fch = int(mat.group('fch'))
                    lch = int(mat.group('lch'))
                    if fch >= maxchans or lch >= maxchans:
                        raise BaseException("chan nb bigger than max chans")
                    vals = list(range(fch,lch+1))
                    onel.extend(vals)
                else:
                    if cval >= maxchans: 
                        raise BaseException("chan nb bigger than max chans")
                    onel.append(int(cval))
            _sums.append(onel)
        return _sums

    def is_saving_done(self):
        if self.save_starting:
            return False

        th_alive = []
        saving_done = True
        for th in self._saving_threads:
            if th.is_alive():
                saving_done = False
                th_alive.append(th)

        self._saving_threads = th_alive
        return saving_done

    def get_full_header(self):
        return "\n".join([f"{ky}={value}" for ky, value in self.headers.items()])

    def reset_header(self):
        keep_d = {}
        for ky in self.header_keep:
            keep_d[ky] = self.headers[ky]
        self.headers = keep_d

    def get_data_info(self, key=None, info=None):
        if info is None:
            info = self.data_info

        if key is not None:
            if key in info:
                return str(info[key])
            else:
                raise BaseException(f'info {key} not found for data')
        
        retstr = ",".join([f'{key}={val}' for key,val in info.items()])
        return retstr

    def get_background_report(self, key=None):
        if self.background_stored is False:
            return f'No background'

        bckg = self.store.get("background")
        info  = bckg.data_info
        curr_timestamp = time.perf_counter()
        secs_ago = int(curr_timestamp - info['timestamp']) 
        preset_str = f"preset (ms)= {info['preset_value']}"
        t = info['time']
        date_str = t.date()
        time_str = f'{t.hour}:{t.minute}:{t.second}'
        ago_str = f"taken at {time_str} on {date_str} / {secs_ago} secs ago"
        return f"{self.detector_name} - {preset_str} - {ago_str}"

    def get_background_info(self, key=None):
        if self.background_stored is False:
            return f'No background stored'

        bckg = self.store.get("background")
        info  = bckg.data_info
        return self.get_data_info(info=info, key=key)

    def get_header(self, name):
        if name in self.headers:
            return self.headers[name]
        else:
            return "unknown header"

    def add_header(self, name, value, keep=False):
        self.headers[name] = str(value)
        if keep:
           if name not in self.header_keep:
               self.header_keep.append(name)

    def remove_header(self, name):
        if name in self.headers:
            self.headers.pop(name)

    #  TODO - OVERWRITE POLICY NOT IMPLEMENTED YET. OVERWRITE WILL HAPPEN

    def rearm_saving(self, reason=''):
        # next save will save from the beginning. with new settings
        self.saved_spectrum_ptr = 0
        print_sdh(f'Rearming because {reason}')
        if reason != 'initacq':
            self.saving_number = 0

    def get_saving_overwrite_policy(self):
        return self.saving_overwrite_policy

    def set_saving_overwrite_policy(self, policy):
        if policy in [SavingOverwritePolicy.ABORT, 
                      SavingOverwritePolicy.OVERWRITE,
                      SavingOverwritePolicy.SAVEALL]:
            self.saving_overwrite_policy = policy
        else:
            raise BaseException("wrong overwrite policy")

    def get_saving_points_per_file(self):
        return self.saving_points_per_file

    def set_saving_points_per_file(self, value):
        value = int(value)
        if self.saving_points_per_file  != value:
            self.saving_points_per_file = value
            self.rearm_saving('points per file')

    def get_saving_next_file_number(self):
        return self.saving_number 

    def set_saving_next_file_number(self, number):
        self.using_xmap_numbers = False
        if number >= 0:
            if self.saving_number != number:
                self.saving_number_set = True
                self.saving_number = number 
        else:
            raise BaseException("saving number must be as positive number")

    def get_saving_index_format(self):
        return self.saving_index_format 

    def set_saving_index_format(self, format):
        if self.saving_index_format != format:
            self.saving_index_format = format
            self.rearm_saving('index_format')

    def get_saving_xmap_numbers(self):
        return self.saving_xmap_numbers 

    def set_saving_xmap_numbers(self, numbers):
        self.saving_xmap_numbers = numbers
        self.using_xmap_numbers = True
        self.saving_number = numbers[2]

    def get_saved_nb_files(self):
        return self.saved_nb_files

    def get_next_file_path(self):

        saving_number = self.saving_index_format % self.saving_number

        prefix = self.get_prefix_and_name()

        if self.saving_xmap_numbers:
            if len(self.saving_xmap_numbers) > 2:
                nums = "_".join( f'{num:04d}' for num in self.saving_xmap_numbers[:-1] )
            else:
                nums = "_".join( f'{num:04d}' for num in self.saving_xmap_numbers )
            number = f'{nums}_{saving_number}'
            filename = f"{prefix}_xiaxx_{number}{self.saving_suffix}"
        else:
            number = saving_number
            filename = f"{prefix}_{number}{self.saving_suffix}"

        return os.path.join(self.get_folder(), filename)

    def get_folder(self):
        return os.path.join(self.saving_root, self.saving_directory)

    def get_saved_file(self):
        return self.saved_filename

    def get_saved_nb_points(self):
        return self.saved_spectrum_ptr

    def check_save(self, saveall=False):

        if self.no_pixels == 0:
            print("nothing to save")
            return False

        if self.no_pixels == self.saved_spectrum_ptr:
            print("all data has been saved")
            return False

        nb_pixels = self.no_pixels
        if not saveall and self.accum_missing:
            # do not save last frame while is not completed
            nb_pixels -=1

        saving_ptr = self.saved_spectrum_ptr
        saving_number = self.saving_number

        try:
            if self.ignore_first:
                 if self.no_pixels > 1 and saving_ptr == 0:
                     saving_ptr = 1

            while nb_pixels >= (saving_ptr + self.saving_points_per_file):
                frnb = saving_ptr
                tonb = saving_ptr + self.saving_points_per_file - 1
                print(f"  Saving spectra {frnb} to {tonb}")
                self._save(from_px=frnb, to_px=tonb, saving_number=saving_number)
                saving_ptr += self.saving_points_per_file
                saving_number += 1

            frnb = saving_ptr

            if saveall:
                if nb_pixels > saving_ptr:
                    tonb = nb_pixels-1
                    print(
                        f"  Saving remaining spectra from {frnb} to {tonb}"
                    )
                    self._save(from_px=frnb, to_px=tonb, saving_number=saving_number)
                    saving_number += 1
                    saving_ptr = nb_pixels
                else:
                    print("  - all data has been already saved. finished")

            self.saved_spectrum_ptr = saving_ptr
            self.saving_number = saving_number

        except BaseException as e:
            import traceback
            traceback.print_exc()
            print("problem while saving {e}")

        return True

    def save(self):
        t0 = time.perf_counter()

        self.save_starting = True
        self.saved_nb_files = 0
        if self.check_save(saveall=True):
            self.wait_save()

        self.save_starting = False

        print(f'saving done. nb files {self.saved_nb_files}')
        elapsed = time.perf_counter() - t0
        print(f"   elapsed to save {elapsed:3.3f} secs")
        return self.saved_nb_files

    def wait_save(self, timeout=3):
        with gevent.Timeout(timeout):
            while True:
                if self.is_saving_done():
                    break
                gevent.sleep(0.1)
        
    def _save(self, from_px=-1, to_px=-1, saving_number=0):
        self.save_starting = True

        # only if this function is called directly (normally it goes through check_save
        # if from_px == -1:
        #     if self.ignore_first:
        #         if self.no_pixels > 1 and self.saved_spectrum_ptr == 0:
        #             self.saved_spectrum_ptr = 1
        #
        #    from_px = self.saved_spectrum_ptr
        #

        if to_px == -1:
            to_px_max = self.no_pixels-1
            if self.saving_points_per_file > 0:
                to_px = from_px + self.saving_points_per_file - 1

            if to_px >= to_px_max or to_px == -1: 
                to_px = to_px_max

        nb_px = to_px-from_px+1

        if nb_px <= 0:
            return

        try:
            dirname = self.get_folder()
            if not os.path.exists(dirname):
                os.makedirs(dirname)
        except IOError as e:
            raise BaseException(f"Cannot create directory {dirname}. Check permissions")


        # correct with pixels in the buffer (if roll has happened)
        fpx = from_px - self.buffer_first_pixel 
        lpx = from_px + nb_px 

        #  Uhmm... headers are copied to avoid possible conflict with concurrent threads 
        #  manipulating headers at the same time
        headers = copy.copy(self.headers)
        headers['xfirstpx']  = from_px
        headers['xlastpx']  = to_px

        # reverse logic
        #    saving_reverse prevails
        #    if not set and saving_auto_reverse is set:
        #        reverse when saving_number is "odd"
        #    in this way, in mapping mode:
        #        set saving_points_per_file to the size of one line
        #        set saving_mode to AUTO
        #        set saving_auto_reverse to True 
        #        odd lines will be saved in reverse order

        reverse = self.saving_reverse
        if not reverse and self.saving_auto_reverse and saving_number%2:
            reverse = True

        nothing_saved = False

        if self.saving_choice in [SavingChoice.BOTH, SavingChoice.CORRECTED]:
            if self.is_background_correction_on():
                if self.multichannel:
                    data = self.get_data(from_px=fpx,to_px=lpx, corrected=True)
                    metadata = self.metadata[:,fpx:lpx,:]
                else:
                    data = self.get_data(from_px=fpx,to_px=lpx, corrected=True)
                    metadata = self.metadata[fpx:lpx,:]

                print(f'  * saving format {self.saving_format} - fpx={fpx} lpx={lpx-1} data={data.shape}')
                self._do_save(data, metadata, headers, saving_number, reverse, data_type="corr")
            else:
                nothing_saved = True

        if nothing_saved or self.saving_choice in [SavingChoice.BOTH, SavingChoice.RAW]:
            if self.multichannel:
                data = self.get_data(from_px=fpx,to_px=lpx, corrected=False)
                metadata = self.metadata[:,fpx:lpx,:]
            else:
                data = self.get_data(from_px=fpx,to_px=lpx, corrected=False)
                metadata = self.metadata[fpx:lpx,:]

            if nothing_saved:
                print(f'    - saving in raw mode as background was not possible')

            print(f'    - saving format {self.saving_format} - fpx={fpx} lpx={lpx-1} data={data.shape}')
            self._do_save(data, metadata, headers, saving_number, reverse, data_type="raw")

    def _do_save(self, data, metadata, headers, saving_number, reverse, data_type="data"):
        if self.saving_format == SavingFormat.EDF:
            print(f"    - using format EDF for type={data_type}")
            # self._save_edf(data, metadata, headers, saving_number, reverse, data_type)
            saving_thread = threading.Thread(target=self._save_edf, \
                                args=[data,metadata,headers,saving_number,reverse, data_type], daemon=True)
            saving_thread.start()
            self._saving_threads.append(saving_thread)
        elif self.saving_format == SavingFormat.EDF_XMAP:
            print(f'    - saving format EDF_XMAP for type={data_type}')
            saving_thread = threading.Thread(target=self._save_edf_xmap, \
                                args=[data,metadata,headers,saving_number,reverse, data_type], daemon=True)
            saving_thread.start()
            self._saving_threads.append(saving_thread)
        elif self.saving_format == SavingFormat.HDF5:
            print(f"    - saving format HDF5 (not implemented)")
            # self._save_hdf5(data, data, metadata, headers, saving_number, reverse, data_type)
        elif self.saving_format == SavingFormat.REDIS:
            print(f"    - saving format REDIS (not implemented)")
            # self._save_redis(data, data, metadata, headers, saving_number, reverse, data_type)

        self.save_starting = False

    def _save_edf_xmap(self, data, metadata, headers, saving_number, reverse, data_type):
        # 
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        headers['xtype'] = 'xmap'
        headers['xmin'] = 0
        headers['xmax'] = self.spectrum_size
        headers['xcorr'] = 0
        headers['xdata'] = 1
        headers['xnb'] = 1
        headers['date'] = now

        # saving a file per channel

        if self.multichannel:
            nb_chans = data.shape[0]
            nb_pixels = data.shape[1]
            nb_vals = data.shape[2]
        else:
            nb_chans = 1
            nb_pixels = data.shape[0]
            nb_vals = data.shape[1]

        print(f'  * saving edf xmap: nb_chans={nb_chans} nb_pixels={nb_pixels} spectrum_size={nb_vals} reverse={reverse}')
       
        saving_number = self.saving_index_format % saving_number

        prefix = self.get_prefix_and_name(data_type=data_type)

        for chno in range(nb_chans):
            if self.multichannel:
                dat_tosave = data[chno,:,:]
            else:
                dat_tosave = data[:,:]

            if self.saving_xmap_numbers:
                if len(self.saving_xmap_numbers) > 2:
                    nums = "_".join( f'{num:04d}' for num in self.saving_xmap_numbers[:-1] )
                else:
                    nums = "_".join( f'{num:04d}' for num in self.saving_xmap_numbers )
                number = f'{nums}_{saving_number}'
            else:
                number = saving_number

            filename = f"{prefix}_xia{chno:02d}_{number}{self.saving_suffix}"

            if chno == 0:
                print(f'  // filename={filename} (does not necessarily correspond with previous messages!!)')

            headers['xdet'] = chno
            if reverse:
               dat_tosave = np.flip(dat_tosave, axis=0)

            self._do_save_xmap(dat_tosave, filename, headers)
            self.saved_nb_files += 1

        # saving a file summing all channels
        if nb_chans > 1 and self.saving_sums is not None:
            if self.saving_sums == "all":
                dat_tosave = np.sum(data, axis=0)

                filename = f"{prefix}_xiaS0_{number}{self.saving_suffix}"

                xdet = ' '.join(map(str, range(nb_chans)))
                headers['xdata'] = 5
                headers['xnb'] = nb_chans
                headers['xdet'] = xdet
                if reverse:
                   dat_tosave = np.flip(dat_tosave, axis=0)
                # the previous header is valid also for stats
                self._do_save_xmap(dat_tosave, filename, headers=copy.copy(headers))
                self.saved_nb_files += 1
            else:
                sumno = 0
                headers['xdata'] = 5
                for sums in self.saving_sums:
                    dat_tosave = np.sum(data[sums], axis=0)
                    filename = f"{prefix}_xiaS{sumno}_{number}{self.saving_suffix}"
                    headers['xnb'] = len(sums)
                    headers['xdet'] = ' '.join(map(str,sums))
                    if reverse:
                       dat_tosave = np.flip(dat_tosave, axis=0)
                    self._do_save_xmap(dat_tosave, filename, headers=copy.copy(headers))
                    self.saved_nb_files += 1
                    sumno += 1

        #  
        # save stats / metadata
        #  
        if self.metadata_size != 0:
            labels = " ".join(self.metadata_labels)
            filename = f"{prefix}_xiast_{number}{self.saving_suffix}"

            # to keep compatibility with old server stats saving: 
            #     1 - swap axes 0 and 1
            #         so that the axes are [pixelno,detno,stats]. 
            #     2 - flatten axis1 and axis2 f
            headers['labels'] = labels
            headers['xdata'] = 3

            for name in ['xmin','xmax']:
                if name in headers:
                    headers.pop(name)

            if self.multichannel:
                # swapaxes 
                #    it is detno,pxno, stat_values
                #    swap to pxno, detno, stat_values
                stats = np.swapaxes(metadata, 0, 1)
                dims = stats.shape
                # flatten axis1 and axis2 (detector - values)
                stats = stats.reshape(dims[0], dims[1]*dims[2])
            else:
                stats = metadata

            if reverse:
                stats = np.flip(stats,axis=0)
            self._do_save_xmap(stats, filename, headers)
            self.saved_nb_files += 1

    def _do_save_xmap(self, data, filename, headers):

        if not fabio_ok:
             print(f'FABIO python module needs to be installed.')
             raise BaseException(f'Cannot save EDF XMAP - Fabio is not installed')

        filename = os.path.join(self.saving_root, self.saving_directory, filename)

        filename = self.saving_overwrite_check(filename)

        img = fabio.edfimage.EdfImage(data)
        img.header = copy.copy(headers)
        # print(f'saving file {filename}')
        img.save(filename)

    def _save_edf(self, data, metadata, headers, saving_number, reverse, data_type):
        #
        # all detectors saved in one file as 2D EDF
        #    reverse is ignored for now
        #
        saving_number = self.saving_index_format % saving_number

        prefix = self.get_prefix_and_name(data_type=data_type)

        filename = f"{prefix}_{saving_number}{self.saving_suffix}"
        filename = os.path.join(self.saving_root, self.saving_directory, filename)
        filename = self.saving_overwrite_check(filename)

        print(f'    - save to file {filename}')

        try:
            img = fabio.edfimage.EdfImage(data)
            img.header = copy.copy(headers)
            img.save(filename)
            self.saved_filename = filename
            self.saved_nb_files += 1
        except BaseException as e:
            raise BaseException(f"cannot save file {filename}: {e}")
        
        # save metadata if any expected
        if self.metadata_size != 0:
            # save metadata in different file
            filename = f"{prefix}_{saving_number}_meta{self.saving_suffix}"
            filename = os.path.join(self.saving_root, self.saving_directory, filename)
            filename = self.saving_overwrite_check(filename)
            try:
                img = fabio.edfimage.EdfImage(data)
                img.header = copy.copy(headers)
                img.save(filename)
                self.saved_nb_files += 1
            except BaseException as e:
                raise BaseException(f"cannot save file {filename}: {e}")

    def _save_hdf5(self, data, metadata, headers, saving_number, reverse, data_type):
        raise BaseException("saving HDF5 not implemented yet")

    def _save_redis(self, data, metadata, headers, saving_number, reverse, data_type):
        raise BaseException("saving REDIS not implemented yet")

    def saving_overwrite_check(self,filename):
        if self.saving_overwrite_policy == SavingOverwritePolicy.ABORT:
            if os.path.exists(filename):
                raise BaseException(f'file {filename} already exists. aborting')
        elif self.saving_overwrite_policy == SavingOverwritePolicy.SAVEALL:
            filebase, ext = os.path.splitext(filename)
            filenb = 1
            while os.path.exists(filename):
                filename = f'{filebase}-file{filenb:03d}{ext}'
                filenb += 1
        return filename
