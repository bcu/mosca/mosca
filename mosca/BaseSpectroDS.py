# -*- coding: utf-8 -*-

import numpy as np
import gevent

from tango.server import Device, DeviceMeta, GreenMode
from tango.server import command, attribute, device_property
from tango.server import Util, run

from tango import DevState

from mosca.Spectrometer import TriggerMode, PresetMode, AcquisitionMode
from mosca.Spectrometer import Saving, SavingFormat, \
          SavingOverwritePolicy, SavingChoice
from mosca.Spectrometer import SpectroDataHandler

def do_gevent():
    gevent.wait(count=1, timeout=0.01)

def run_server(srv_class):
    import sys

    py = Util(sys.argv)
    py.server_set_event_loop(do_gevent)
    run((srv_class,))

def print_bsds(msg):
    print(f"[MOSCA BSDS]:{msg}")

class BaseSpectroDS(Device):
    __classmeta__ = DeviceMeta

    green_mode = GreenMode.Gevent
    is_multichannel = False

    save_ignore_first = device_property(dtype=int, default_value=0)
    default_readout_time = device_property(dtype=float, default_value=0)

    detname = device_property(dtype=str, default_value="")

    default_detector_name = "mosca"

    def __init__(self, *args, **kwargs):
        print_bsds("BaseSpectroDS __init__")

        self._preset_value = 1.0
        self._spectrum_size = 20
        self._number_channels = 1
        self._readout_time = 0

        self._acq_modes = [
            AcquisitionMode.SINGLE,
            AcquisitionMode.CONCATENATION,
            AcquisitionMode.ACCUMULATION,
        ]

        self._trigger_modes = [
            TriggerMode.SOFTWARE,
        ]
        self._trigger_mode = TriggerMode.SOFTWARE

        self._preset_modes = [
            PresetMode.REALTIME,
        ]
        self._preset_mode = PresetMode.REALTIME

        self._number_points = 1

        self.data_handler = SpectroDataHandler()
        self.data_handler.multichannel = self.is_multichannel

        # why does it call again SpectroDataHandler.init_data() ?
        Device.__init__(self, *args, **kwargs)
        self.set_idle()

    def init_device(self):
        print_bsds("in init_device()")
        Device.init_device(self)
        self.data_handler.set_ignore_first(self.save_ignore_first)
        print_bsds(f'Default readout time is {self.default_readout_time}')
        self.set_readout_time(self.default_readout_time)

        if not self.detname:
            self._detector_name = self.default_detector_name
        else:
            self._detector_name = self.detname

        self.data_handler.set_detector_name(self._detector_name)

    @command
    def prepareAcq(self):
        self.prepare_acquisition()

    @command
    def startAcq(self):
        self.start_acquisition()

    @command
    def stopAcq(self):
        self.stop_acquisition()

    # command or attribute ?
    @command(dtype_in=None, dtype_out=[int])
    def getAcqStatus(self):
        ds_state = self.get_state()
        if ds_state in (DevState.STANDBY, DevState.ON, DevState.INIT):
            _state = 0
        elif ds_state == DevState.RUNNING:
            _state = 1
        elif ds_state == DevState.FAULT:
            _state = 2  # ???
        else:
            print(f'State is {ds_state}')
            _state = 4  # unknown state

        _read_pixel = self.get_acquired_points()
        _hw_curr_pixel = self.get_hw_current_pixel()
        _saved_pixel = self.data_handler.get_saved_nb_points()
        return [_state, _read_pixel, _saved_pixel, _hw_curr_pixel]

    @command
    def abortAcq(self):
        print_bsds("MOCKUP aborting")
        self.abort_acquisition()

    @command(dtype_in=[str])
    def addCounter(self, argin):
        self.data_handler.add_counter(*argin)

    @command
    def resetCounters(self):
        self.data_handler.init_counters()

    @command(dtype_in=str, dtype_out=str)
    def getCounterInfo(self, cnt_name):
        return self.data_handler.get_counter_info(cnt_name)

    @command(dtype_out="DevVarStringArray")
    def getAllCounters(self):
        return self.data_handler.get_all_counter_info()

    @command(dtype_in=str, dtype_out="DevVarLongArray")
    def getCounterValue(self, counter_name):
        return self.data_handler.get_counter_value(counter_name)

    @command(dtype_in=[int], dtype_out=[int])
    def getCounterValues(self, argin):

        if len(argin) > 0:
            frompx = argin[0]
        else:
            frompx = 0

        if len(argin) > 1:
           topx = argin[1]
        else:
           topx = None

        return self.data_handler.get_counter_values(frompx=frompx, topx=topx, flatten="counter")

    @command(dtype_in=[int],dtype_out=[float])
    def getMetadataValues(self, argin):

        if len(argin) > 0:
            frompx = argin[0]
        else:
            frompx = 0

        if len(argin) > 1:
           topx = argin[1]
        else:
           topx = -1

        return self.data_handler.get_metadata(from_px=frompx, to_px=topx, orderby='det', flatten=True)

    @command(dtype_in=[str,str])
    def setCounterDetector(self,argin):
        counter_name, detector = argin
        return self.data_handler.set_counter_detector(counter_name, detector)
    
    @command(dtype_in=[int],dtype_out=[np.uint32])
    def getData(self, argin):

        if len(argin) > 0:
            frompx = argin[0]
        else:
            frompx = 0

        if len(argin) > 1:
           topx = argin[1]
        else:
           topx = -1

        return self.data_handler.get_data(from_px=frompx, to_px=topx, flatten=True)

    @command(dtype_out=[int])
    def getBackgroundData(self):
        return self.data_handler.get_background_data(flatten=True)

    @command(dtype_in=str, dtype_out=str)
    def getDataInfo(self, info_key):
        return self.data_handler.get_data_info(info_key)

    @command(dtype_in=str, dtype_out=str)
    def getBackgroundInfo(self, info_key):
        return self.data_handler.get_background_info(info_key)

    @command
    def storeBackground(self):
        self.data_handler.store_background()

    @command
    def saveData(self):
        self.data_handler.save()

    @command
    def saveBackground(self):
        self.data_handler.save_background()

    @attribute(dtype=str, doc="short string identifying the detector model")
    def detector_model(self):
        return self.get_model()

    @attribute(dtype=str, doc="string will full detector information")
    def detector_info(self):
        return self.get_info()

    @attribute(dtype=str, doc="name for this detector")
    def detector_name(self):
        return self._detector_name

    @detector_name.write
    def detector_name(self, value):
        self._detector_name = value
        self.data_handler.set_detector_name(self._detector_name)

    @attribute(dtype=float, doc="preset time in msecs")
    def preset_value(self):
        return float(self.get_preset_value())

    @preset_value.write
    def preset_value(self, value):
        self.set_preset_value(value)

    @attribute(dtype=float)
    def readout_time(self):
        return self.get_readout_time()

    @readout_time.write
    def readout_time(self, value):
        self.set_readout_time(value)

    @attribute(dtype=int)
    def spectrum_size(self):
        return self.get_spectrum_size()

    @attribute(dtype=str)
    def trigger_mode(self):
        trigger_mode = self.get_trigger_mode()
        return trigger_mode.name

    @trigger_mode.write
    def trigger_mode(self, value):
        isvalue = isname = False
        try:
            val = int(value)
            isvalue = True
        except ValueError:
            isname = True
            value = value.upper()

        for mode in self._trigger_modes:
            if isvalue and mode.value == val:
                self.set_trigger_mode(mode)
                break
            if isname and mode.name.startswith(value):
                self.set_trigger_mode(mode)
                break
        else:
            raise BaseException("trigger mode not supported")

    @attribute(dtype=[str], max_dim_x=10)
    def supported_trigger_modes(self):
        return [mode.name for mode in self._trigger_modes]

    @attribute(dtype=str)
    def preset_mode(self):
        return self.get_preset_mode().name

    @preset_mode.write
    def preset_mode(self, value):
        isvalue = isname = False
        try:
            val = int(value)
            isvalue = True
        except ValueError:
            isname = True
            value = value.upper()

        for mode in self._preset_modes:
            if isvalue and mode.value == val:
                self.set_preset_mode(mode)
                break
            if isname and mode.name.startswith(value):
                self.set_preset_mode(mode)
                break
        else:
            raise BaseException("preset mode not supported")

    def get_preset_mode(self):
        return self._preset_mode

    def set_preset_mode(self, mode):
        self._preset_mode = mode

    @attribute(dtype=[str], max_dim_x=10)
    def supported_preset_modes(self):
        return [mode.name for mode in self._preset_modes]

    @attribute(dtype=bool)
    def multichannel(self):
        return self.data_handler.multichannel

    @attribute(dtype=int)
    def number_channels(self):
        return self.get_number_channels()

    @attribute(dtype=int)
    def number_points(self):
        return self.get_number_points()

    @number_points.write
    def number_points(self, value):
        self.set_number_points(value)

    @attribute(dtype=int)
    def acq_nb_points(self):
        return self.data_handler.get_number_pixels()

    @attribute(dtype=int)
    def saved_nb_files(self):
        return self.data_handler.get_saved_nb_files()

    @attribute
    def saved_nb_points(self):
        return self.data_handler.get_saved_nb_points()

    # max return size is 40000 spectra (in case of HamaSpectro
    @attribute(dtype=[int], max_dim_x=10240000)
    def data(self):
        return self.data_handler.get_data(flatten=True)

    @attribute(dtype=str)
    def data_info(self):
        return self.data_handler.get_data_info()

    @attribute(dtype=str)
    def background_info(self):
        return self.data_handler.get_background_info()

    @attribute(dtype=str)
    def background_report(self):
        return self.data_handler.get_background_report()

    @attribute(dtype=[str], max_dim_x=100)
    def metadata_labels(self):
        return self.data_handler.get_metadata_labels()

    @attribute(dtype=[int], max_dim_x=16384000)
    def metadata(self):
        return self.get_metadata(flatten=True)

    @attribute(dtype=(str,), max_dim_x=8192)
    def counter_names(self):
        return self.data_handler.get_counter_names()

    @attribute(dtype=(int,), max_dim_x=1024000)
    def counter_values(self):
        return self.data_handler.get_counter_values()

    @attribute(dtype=str)
    def acq_mode(self):
        return self.get_acquisition_mode().name

    @acq_mode.write
    def acq_mode(self, value):
        isvalue = isname = False
        try:
            val = int(value)
            isvalue = True
        except ValueError:
            isname = True
            value = value.upper()

        for mode in self._acq_modes:
            if isvalue and mode.value == val:
                self.set_acquisition_mode(mode)
                break
            if isname and mode.name.startswith(value):
                self.set_acquisition_mode(mode)
                break
        else:
            raise BaseException("acquisition mode not supported")

    @attribute(dtype=int)
    def accum_nb_frames(self):
        return self.get_accum_nb_frames()

    @accum_nb_frames.write
    def accum_nb_frames(self, value):
        self.set_accum_nb_frames(value)

    @attribute(dtype=str, doc="file prefix for saving spectra")
    def saving_prefix(self):
        return self.data_handler.get_saving_prefix()

    @saving_prefix.write
    def saving_prefix(self, value):
        self.data_handler.set_saving_prefix(value)

    @attribute(dtype=str)
    def saving_suffix(self):
        return self.data_handler.get_saving_suffix()

    @saving_suffix.write
    def saving_suffix(self, value):
        self.data_handler.set_saving_suffix(value)

    @attribute(dtype=str)
    def saving_directory(self):
        return self.data_handler.get_saving_directory()

    @saving_directory.write
    def saving_directory(self, value):
        self.data_handler.set_saving_directory(value)

    @attribute(dtype=str)
    def saving_root(self):
        return self.data_handler.get_saving_rootpath()

    @saving_root.write
    def saving_root(self, value):
        self.data_handler.set_saving_rootpath(value)

    @attribute(dtype=str)
    def saving_format(self):
        return self.data_handler.get_saving_format().name

    @saving_format.write
    def saving_format(self, save_format):
        for _format in SavingFormat:
            if save_format == _format.name:
                self.data_handler.set_saving_format(_format)
                break
        else:
            raise BaseException("saving formart not supported")

    @attribute(dtype=str)
    def saving_mode(self):
        return self.data_handler.get_saving_mode().name

    @saving_mode.write
    def saving_mode(self, mode):
        for _mode in [Saving.MANUAL, Saving.AUTO]:
            if mode == _mode.name:
                self.data_handler.set_saving_mode(_mode)
                break
        else:
            raise BaseException("saving mode not supported")

    @attribute(dtype=int)
    def saving_ignore_first(self):
        return self.data_handler.get_ignore_first()

    @saving_ignore_first.write
    def saving_ignore_first(self, value):
        self.data_handler.set_ignore_first(value)

    @attribute(dtype=str)
    def saving_auto_reverse(self):
        return self.data_handler.get_saving_auto_reverse()

    @saving_auto_reverse.write
    def saving_auto_reverse(self, reverse):
        return self.data_handler.set_saving_auto_reverse(reverse)

    @attribute(dtype=str)
    def saving_reverse(self):
        return self.data_handler.get_saving_reverse()

    @saving_reverse.write
    def saving_reverse(self, reverse):
        return self.data_handler.set_saving_reverse(reverse)

    @attribute(dtype=int)
    def saving_next_number(self):
        return self.data_handler.get_saving_next_file_number()

    @saving_next_number.write
    def saving_next_number(self, value):
        self.data_handler.set_saving_next_file_number(value)

    @attribute(dtype=str)
    def saving_index_format(self):
        return self.data_handler.get_saving_index_format()

    @saving_index_format.write
    def saving_index_format(self, value):
        self.data_handler.set_saving_index_format(value)

    @command(dtype_in=[str])
    def addHeader(self, argin):
        name = argin[0]
        value = ",".join(argin[1:])
        self.data_handler.add_header(name, value)

    @command(dtype_in=str)
    def getHeader(self, name):
        self.data_handler.get_header(name)

    @attribute(dtype=str)
    def full_header(self):
        return self.data_handler.get_full_header()

    @attribute(dtype=int)
    def saving_points_per_file(self):
        return self.data_handler.get_saving_points_per_file()

    @saving_points_per_file.write
    def saving_points_per_file(self, value):
        self.data_handler.set_saving_points_per_file(value)

    @attribute(dtype=str)
    def saving_overwrite_policy(self):
        return self.data_handler.get_saving_overwrite_policy().name

    @saving_overwrite_policy.write
    def saving_overwrite_policy(self, policy):
        for _policy in [SavingOverwritePolicy.ABORT, 
                        SavingOverwritePolicy.OVERWRITE,
                        SavingOverwritePolicy.SAVEALL]:
            polup = policy.upper()
            if _policy.name.startswith(polup):
                self.data_handler.set_saving_overwrite_policy(_policy)
                break
        else:
            raise BaseException("overwrite policy not supported")

    @attribute(dtype=str)
    def saving_choice(self):
        return self.data_handler.get_saving_choice().name

    @saving_choice.write
    def saving_choice(self, choice):
        for _choice in [SavingChoice.RAW, 
                        SavingChoice.CORRECTED,
                        SavingChoice.BOTH]:
            chup = choice.upper()
            if _choice.name.startswith(chup):
                self.data_handler.set_saving_choice(_choice)
                break
        else:
            raise BaseException("saving choice not supported")

    @attribute(dtype=str)
    def saved_filename(self):
        return self.data_handler.get_saved_file()

    @attribute(dtype=str)
    def saving_next_file(self):
        return self.data_handler.get_next_file_path()

    @attribute(dtype=bool)
    def saving_done(self):
        return self.data_handler.is_saving_done()

    @attribute(dtype=str)
    def saving_sums(self):
        """
        Use during edf_xmap saving to decide on how to save
        sum data.
        default - is "all"
         sumspecs example could be:
           none
              no sum saved during edf_xmap
           all
              sum of all detchannels saved during edf_xmap labeled as s0
           0,3;5-8;10,11,13  
              in this case three sums will be generated at saving edf_xmap
                 label s0 - will saved sum of channels 0 and 3
                 label s1 - will saved sum of all channels 5 through 8
                 label s2 - will saved sum of channels 10,11 and 13
        """
        return self.data_handler.get_saving_sumspec()

    @saving_sums.write
    def saving_sums(self,sumspec):
        self.data_handler.set_saving_sumspec(sumspec)

    # binning

    # live_mode

    # calibration

    ##  / yes or no if dark available (valid if same exposure_time)
    @attribute(dtype=bool)
    def correct_background(self):
        return self.data_handler.is_background_correction_on()

    @correct_background.write
    def correct_background(self, value):
        if value:
            self.data_handler.correct_background_on()
        else:
            self.data_handler.correct_background_off()
    #
    # Functions that the derived class can implement
    #
    def get_model(self):
        return "mockup"

    def get_info(self):
        return "mockup spectro info"

    def get_number_channels(self):
        return self._number_channels

    def get_spectrum_size(self):
        return self._spectrum_size

    def get_trigger_mode(self):
        return self._trigger_mode

    def set_trigger_mode(self, value):
        self._trigger_mode = value

    def get_preset_value(self):
        return self._preset_value

    def set_preset_value(self, value):
        self._preset_value = value

    def get_number_points(self):
        return self._number_points

    def get_readout_time(self):
        return self._readout_time

    def set_readout_time(self, value):
        self._readout_time = value

    def get_acquired_points(self):
        return self.data_handler.get_number_pixels()

    def get_hw_current_pixel(self):
        # if not implemented by specific server returns the same as previous.
        # if relevant... implement the value in specific code
        return self.get_acquired_points()

    def set_number_points(self, value):
        self._number_points = value

    def prepare_acquisition(self):
        print("MOCKUP preparing")

    def start_acquisition(self):
        print("MOCKUP starting")
        self.set_running()

    def stop_acquisition(self):
        print("MOCKUP stopping")
        self.set_idle()

    def abort_acquisition(self):
        print("MOCKUP aborting")
        self.set_idle()

    # Management of state at DS level.
    def set_running(self):
        self.set_state(DevState.RUNNING)

    def set_idle(self):
        self.set_state(DevState.STANDBY)

    def set_fault(self):
        self.set_state(DevState.FAULT)

    def set_init_state(self):
        self.set_state(DevState.INIT)

    # data management calls
    def get_data(self, flatten=True):
        return self.data_handler.get_data(flatten=flatten)

    def get_metadata(self, flatten=True):
        return self.data_handler.get_metadata(flatten=flatten)

    def get_acquisition_mode(self):
        return self.data_handler.get_acquisition_mode()

    def set_acquisition_mode(self, value):
        self.data_handler.set_acquisition_mode(value)

    def get_accum_nb_frames(self):
        return self.data_handler.get_accum_nb_frames()

    def set_accum_nb_frames(self, value):
        return self.data_handler.set_accum_nb_frames(value)
