#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

OceanOptics spectrometers interfacing via python-SeaBreeze module.
tested with QE-Pro

"""

import time
import numpy as np
import threading

from mosca.Spectrometer import SpectroDataHandler, TriggerMode

# Seabreeze library.
import seabreeze
seabreeze.use("cseabreeze")
from seabreeze.spectrometers import Spectrometer

# Acquisition status
# INITIALIZING  READY   RUNNING   FAULT
STATE_INIT, STATE_IDLE, STATE_RUNNING, STATE_FAULT = (0, 1, 2, 3)

"""
 Trigger number depends on OO spectrometer model.

 Available triggering modes and numbering depend on OO model.
        ex:
                                      HR2000+    HR4000      QEPRO
        - Normal (Free Run)             0          0          0
        - Software Trigger Mode         1          1
        - EHLTM                         2          2          1
        - ESTM                          3                     2*
        - Ext Hwd Edge Trigger Mode     4          4          3
        - NSM                                      3

        *: not implemented

  For now only mode 0 (SOFTWARE) and 3 (SYNC) are implemented
"""
OO_TRIGGER_SOFTWARE = 0
OO_TRIGGER_SYNC = 3

# Errors
OO_NOT_FOUND, OO_FEATURE_NOT_AVAILABLE, \
  OO_WRONG_PRESET, OO_TRIGGER_MODE = range(4)

OCEAN_OPTICS_ERRORS = {
   OO_NOT_FOUND: "device not found",
   OO_FEATURE_NOT_AVAILABLE: "feature not available",
   OO_WRONG_PRESET: "preset time invalid",
   OO_TRIGGER_MODE: "cannot set trigger mode",
}
OCEAN_OPTICS_DEFAULT_ERROR = "unidentified error"

class OceanOpticsError(BaseException):
    def __init__(self, errno, errstr=None, err_desc=None):
        self.errno = errno
        if errno in OCEAN_OPTICS_ERRORS:
            error_str = OCEAN_OPTICS_ERRORS[errno]
        else:
            error_str = OCEAN_OPTICS_DEFAULT_ERROR

        self.strerror = errstr is None and error_str or errstr

        self.description = err_desc 
        self.args = errno, error_str, err_desc

    def __str__(self):
        s = "[OceanOpticsError {}] {}".format(self.errno, self.strerror)
        if self.description:
            s += ": {}".format(self.description)
        return s

class OceanDevice():
    """
    Control Object dor spectrometer.
    """

    vendor = "OceanOptics"
    library = 'seabreeze'
    backend = 'cseabreeze'

    connect_timeout = 10

    libvers = seabreeze.__version__
    
    initialized = False

    def __init__(self, serialno=None):

        self.spec_info = None

        self.serialno = serialno

        self.data_handler = SpectroDataHandler()

        self.acq_started = False
        self.acq_t0 = 0

        self.exposure_time = 10 # in ms
        self.nb_acq_frames = 0
        self.spectrum_size = 0
        self.module_info = None

        self.buffer_minsize = self.buffer_maxsize = None

        self.buffer = None
        self.spect = None
        self.device = None
        self.th_lock = threading.Lock()

        self.state = STATE_INIT
        self.stop_it = False
        self.connected = False   

        # defaults
        self.number_frames = 1
        self.trigger_mode = TriggerMode.SOFTWARE

        # default values of valid spectrum margin
        self.left_margin = 10
        self.right_margin = -10

        #  IMPORTANT NOTE ON TIMING
        #
        #   when using trigger mode SYNC 
        #   on a receiving a new trigger a cycle of "exposure_time" is
        #   started, this is followed by the ncessary time for readout. 
        #   Any new trigger that arrives during the cycle wil be ignored
        # 
        #  For this reason the exposure_time has to be programmed to be
        #   short enough not to miss triggers.
        #  An alternative is to program a readout time for the device. 
        #   In that case the readout time value will be automatically substracted 
        #  from the exposure time when in SYNC trigger_mode 
        #
        self.readout_time = 0
        self.minimum_exposure_time = 8000 # in us

        # try connection
        try:
            self.connect()
        except BaseException as e:
            print(e)

    def connect(self):
        """
        Hardware connection
        Initialization
        """
        if not self.initialized:
            msg = f"Timeout connectng to spectrometer" 
            if self.serialno is not None:
                try:
                    self.device = Spectrometer.from_serial_number(self.serialno)
                except BaseException as e:
                    raise OceanOpticsError(OO_NOT_FOUND, err_desc=f'serial_number={self.serialno}')
            else:
                try:
                    self.device = Spectrometer.from_serial_number()
                except BaseException as e:
                    raise OceanOpticsError(OO_NOT_FOUND, err_desc=f'no OceanOptics device found')

            if self.serialno is None:
                self.serialno = self.device.serial_number

            self.buffer = self.device.f.data_buffer
            self.spect = self.device.f.spectrometer

            self.initialized = True

        self.connected = True   
        self.update_info()

        self.state = STATE_INIT

    def disconnect(self):
        try:
            self.device.close()
            self.initialized = False
            self.connected = False
        except Exception as close_err:
            raise RuntimeError("Cannot close device") from close_err

    def is_connected(self):
        return self.connected

    def get_model(self):
        return self.model

    def get_info(self):
        return self.module_info

    def update_info(self):
        """
        Return info (product, serial, firmware, driver, library) got from device.
        """
        if not self.connected:
            raise BaseException("not connected")

        if len(self.device.features['thermo_electric']) > 0:
            self.has_thermo_electric = True
        else:
            self.has_thermo_electric = False

        min_us , max_us = self.spect.get_integration_time_micros_limits()
        self.min_exptime_ms, self.max_exptime_ms =  min_us/1000.0, max_us/1000.0
        self.read_buffer_limits()

        self.spectrum_raw_size = self.device.pixels 
        self.calc_spectrum_size()


        self.model = f'{self.vendor} {self.device.model}'

        info_str = f"{self.model} - serial:{self.serialno}"
        info_str += f"lib:{self.library}-{self.libvers} backend:{self.backend}"

        self.module_info = info_str
      
    def get_temperature(self):
        """
        Return info about state of the controller:
        * temperature
        * status
        """
        if self.has_thermo_electric:
            with self.th_lock:
                temp = self.device.f.thermo_electric.read_temperature_degrees_celsius()
            return temp
        else:
            raise OceanOpticsErrors(OO_FEATURE_NOT_AVAILABLE, err_desc="(temperature)")

    def get_spectrum_size(self):
        return self.spectrum_size

    def get_number_frames(self):
        """
        Return int: number of frames to acquire.
        """
        return self.number_frames

    def set_number_frames(self, value):
        self.number_frames = value

    def get_current_frame(self):
        """
        Return int: number of frames acquired.
        """
        return self.nb_acq_frames
    
    def get_trigger_mode(self):
        return self.trigger_mode

    def set_trigger_mode(self, trigger_mode):
        self.trigger_mode = trigger_mode
        self._set_trigger_mode()

    def _set_trigger_mode(self):
        trigger_mode = self.trigger_mode

        with self.th_lock:
            try:
                if trigger_mode == TriggerMode.SOFTWARE:
                    self.spect.set_trigger_mode(OO_TRIGGER_SOFTWARE)
                elif trigger_mode == TriggerMode.SYNC:
                    self.spect.set_trigger_mode(OO_TRIGGER_SYNC)
                else:
                    raise OceanOpticsError(OO_TRIGGER_MODE, err_desc=f'only sync and software supported')
            except BaseException as trig_err:
                raise OceanOpticsError(OO_TRIGGER_MODE, err_desc=f'cannot set trigger mode {str(trig_err)}')

        self.set_exposure_time()

    def get_exposure_time(self):
        return self.exposure_time

    def set_exposure_time(self, exptime_ms=None):
        """
        Set exposure/integration time to <exptime_ms> * 1000 micro-seconds.
        <exptime_ms> : int : exposure time in milliseconds.
        """
        if exptime_ms is None:
            exptime_ms = self.get_exposure_time()

        time_ms = exptime_ms

        readout_ms = self.readout_time 
        if self.get_trigger_mode() == TriggerMode.SYNC:
            time_ms -= readout_ms
        time_us = time_ms * 1000

        if not self.min_exptime_ms < time_ms < self.max_exptime_ms:
            raise OceanOpticsError(OO_WRONG_PRESET, 
                  err_desc=f'limits (min={self.min_exptime_ms:3.2f} / max={self.max_exptime_ms:3.2f}')
        
        with self.th_lock:
            self.spect.set_integration_time_micros(time_us)
        self.exposure_time = exptime_ms

    def get_readout_time(self):
        return self.readout_time 

    def set_readout_time(self,value):
        self.readout_time = value
        self.set_exposure_time()

    """
    Buffer
    """
    def read_buffer_limits(self):
        """
        Update and return buffer parameters values:
        current, min, max
        """
        if self.buffer_minsize is None:
            with self.th_lock:
                self.buffer_minsize = self.buffer.get_buffer_capacity_minimum()
                self.buffer_maxsize = self.buffer.get_buffer_capacity_maximum()

    def get_buffer_size(self):
        """
        Return number of spectrum that buffer can contain.
        """
        with self.th_lock:
            return self.buffer.get_buffer_capacity()

    def set_buffer_size(self, bufsize):
        """
        """
        if bufsize > self.buffer_maxsize:
            bufsize = bufmax

        with self.th_lock:
            self.buffer.set_buffer_capacity(bufsize)

    """
    Data acquistion
    """
    def get_data(self):
        return self.data_handler.get_data()

    def start_acquisition(self):
        """
        Executed on 'startAcq' DS command.
        """
        try:
            self.state = STATE_RUNNING

            info = {
                'preset_value': self.exposure_time,
                'preset_real': self.exposure_time - self.readout_time,
                'readout_time': self.readout_time,  
                'detector_model': self.model,
            }

            self.data_handler.init_data(spectrum_size=self.spectrum_size, data_info=info)

            # self.data_handler.set_metadata_labels(['time'])

            self.set_exposure_time(self.exposure_time)
            self.set_buffer_size(self.number_frames)
            self._set_trigger_mode()

            self.nb_acq_frames = 0
     
            with self.th_lock:
                self.buffer.clear()

            self.stop_it = False
            self.acq_t0 = time.perf_counter()

            print(f"\n  start_acquisition nframes={self.number_frames} exptime={self.exposure_time} trig={self.trigger_mode}")
            self.task1 = threading.Thread(target=self._acquisition_thread, daemon=True)
            self.task1.start() 
        except BaseException as acq_err:
            self.state = STATE_IDLE
            raise RuntimeError(f"error while starting acquisition {acq_err}") from acq_err

    def set_data_margins(self, left_margin, right_margin=None):
        if right_margin is None:
            self.right_margin = -left_margin
        else:
            self.right_margin = right_margin

        self.left_margin = left_margin

        if self.connected:
            self.calc_spectrum_size()

    def get_data_margins(self):
        return self.left_margin, self.right_margin

    def calc_spectrum_size(self):
        if self.right_margin < 0:
            self.right_margin = self.spectrum_raw_size + self.right_margin

        self.spectrum_size = self.right_margin - self.left_margin 
        self.first_ch = self.left_margin
        self.last_ch = self.left_margin + self.spectrum_size

    def acquisition_finished(self):
        print(f"  acquisition_finished.")
        self.data_handler.data_completed()
        self.state = STATE_IDLE

    def stop_acquisition(self, t1=None):
        if self.is_acquiring():
            self.stop_it = True
        else:
            ret = self._stop_acquisition()
            if ret:
                self.state = STATE_IDLE
            else:
                self.state = STATE_FAULT

    def abort_acquisition(self):
        self.stop_acquisition()

    def _stop_acquisition(self):
        self.stop_it = True # to tell greenlets to stop reading
        self.stopped = True  # to avoid reading any new data
        self.state = STATE_IDLE

    def read_data(self, spectrum_cnt=1):
        """
        Return <spectrum_cnt> spectra and timestamps read from controller as flat numpy arrays.
        Only used by OOSpectro object in acquisition loops.

        use get_data() to obtain the acquired data
        """
        data_size = self.spectrum_size * spectrum_cnt  

        data = np.zeros(data_size, dtype=np.uint32)
        times = np.zeros(spectrum_cnt, dtype=np.float64)

        for spectrum_idx in range(spectrum_cnt):
            # SPECTRA
            # spectrum = self.spect.get_intensities(correct_dark_counts=False, correct_nonlinearity=False)[10:-10]
            spectrum = self.spect.get_intensities()
            # restrict between valid margins
            spectrum = spectrum[self.first_ch:self.last_ch]

            idx_beg = spectrum_idx * self.spectrum_size
            idx_end = ( (spectrum_idx + 1 ) * self.spectrum_size ) -1
            data[idx_beg:idx_end+1] = spectrum

            # TIMESTAMPS
            times[spectrum_idx] = time.perf_counter() - self.acq_t0

        return data, times

    def _acquisition_thread(self):
        """
        Read spectra from hardware controller until <nframes> have been acquired.
        """
        nframes = self.number_frames

        print(f"  - acquisition_thread started for {nframes} frames\n")

        time.sleep(0.02)

        t0 = 0

        acquired = 0       # number of spectrum acquiered by hwr controller.
        remaining = nframes  # number of spectrum still to be acquiered and read.
        self.nb_acq_frames = 0

        while self.nb_acq_frames <= nframes:
            time.sleep(0.02)

            if self.stop_it:
                print("stop required by software")
                self._stop_acquisition()
                time.sleep(0.01)
                break

            with self.th_lock:
                acquired = self.buffer.get_number_of_elements()

            if acquired > 0:
                if t0 == 0:
                    t0 = time.perf_counter()

                with self.th_lock:
                    d, t = self.read_data(acquired)

                    # Split flat data array into arrays of size of one spectrum.
                    d = d.reshape(acquired, self.spectrum_size)
                    t = t.reshape(acquired,1)
    
                    self.nb_acq_frames += acquired
                    remaining -= acquired

                    #self.data_handler.add_spectra(d, metadata=t)
                    self.data_handler.add_spectra(d)

                if remaining == 0:
                    break

            if self.nb_acq_frames % 5 == 0:   # print only 1 frame on 5
                print("  - %d/%d frames acquired    " % (self.nb_acq_frames, nframes), end='\r')

        if self.nb_acq_frames > 1:
            elapsed = time.perf_counter() - t0
            per_frame = elapsed / (self.nb_acq_frames-1) * 1000
            print("  - %d/%d frames acquired    " % (self.nb_acq_frames, nframes), end='\n')
            print(
                "  total elapsed time is %3.2f msecs / %3.2f msecs per frame"
                % (elapsed * 1000, per_frame)
            )

        self.acquisition_finished()

    def is_init(self):
        return self.state == STATE_INIT

    def is_acquiring(self):
        return self.state == STATE_RUNNING

    is_running = is_acquiring

    def is_idle(self):
        return self.state == STATE_IDLE

    def is_fault(self):
        return self.state == STATE_FAULT
