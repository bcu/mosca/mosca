
from tango.server import run, command, attribute
# from tango.server import attribute, device_property

from mosca.BaseSpectroDS import BaseSpectroDS
from mosca.devices.simulation.SimulSpectro import SimulSpectro

class SimulSpectroDS(BaseSpectroDS):

    default_detector_name = "simul"

    def init_device(self): 
        self.spectro = SimulSpectro()
        super().init_device()

    def always_executed_hook(self):
        if self.spectro.is_acquiring():
            self.set_running()
        else:
            self.set_idle()

    @command(dtype_in=int)
    def setNumberModules(self, value):
        self.spectro.number_modules = value

    @attribute(dtype=int)
    def spectrum_size(self):
        return self.spectro.spectrum_size 

    @spectrum_size.write
    def spectrum_size(self, value):
        self.spectro.spectrum_size = value

    @attribute(dtype=bool)
    def add_random(self):
        return self.spectro.add_random 

    @add_random.write
    def add_random(self, value):
        self.spectro.add_random = value

    def get_number_channels(self):
        return self.spectro.number_modules

    def get_spectrum_size(self):
        return self.spectro.spectrum_size

    def get_number_points(self):
        return self.spectro.number_frames

    def set_number_points(self, value):
        self.spectro.number_frames = value

    def get_acquired_points(self):
        return self.spectro.acquired_frames

    def get_model(self):
        return self.spectro.get_model()

    def get_info(self):
        return self.spectro.get_info()

    def get_preset_value(self):
        return self.spectro.preset_value

    def set_preset_value(self, value):
        self.spectro.preset_value = value

    def get_trigger_mode(self):
        return self.spectro.trigger_mode

    def set_trigger_mode(self, mode):
        self.spectro.trigger_mode = mode

    def get_readout_time(self):
        return self.spectro.readout_time

    def set_readout_time(self, value):
        self.spectro.readout_time = value

    def prepare_acquisition(self):
        self.spectro.prepare_acquisition()

    def start_acquisition(self):
        self.spectro.start_acquisition()

    def abort_acquisition(self):
        self.spectro.abort_acquisition()

    def stop_acquisition(self):
        self.spectro.stop_acquisition()


def run_it():
    run((SimulSpectroDS,))

if __name__ == "__main__":
    run_it()
