from tango.server import run
from tango.server import attribute, device_property

from mosca.devices.hamamatsu.HamaSpectro import HamaSpectro, HamaCaptureMode, HamaTriggerMode

from mosca.Spectrometer import TriggerMode
from mosca.BaseSpectroDS import BaseSpectroDS

class HamaSpectroDS(BaseSpectroDS):

    minimum_cycle_overtime = device_property(dtype=str, default_value=2000)
    device_index = device_property(dtype=str, default_value=0)

    default_detector_name = "hama"

    def init_device(self):

        BaseSpectroDS.init_device(self)

        self._trigger_modes = [TriggerMode.SOFTWARE, TriggerMode.SYNC, TriggerMode.GATE]
        self.spectro = HamaSpectro()
        self.spectro.connect(self.device_index)
        self.spectro.minimum_cycle_overtime = self.minimum_cycle_overtime
        self.spectro.readout_time = BaseSpectroDS.get_readout_time(self) # program default from DS property

        print(
            f"Initializing Hama spectro server {self.get_name()} ; device_index={self.device_index}"
        )

    def always_executed_hook(self):
        self.update_state()
    
    def update_state(self):
        if self.spectro is None:
            self.set_init_state()
        elif self.spectro.is_acquiring():
            self.set_running()
        elif self.spectro.is_idle():
            self.set_idle()
        else:
            self.set_fault()

    # BEGIN HamaSpectro specific attributes
    @attribute(dtype=float)
    def preset_cycle(self):
        return self.spectro.preset_cycle

    @preset_cycle.write
    def preset_cycle(self, value):
        self.spectro.preset_cycle = value

    @attribute
    def data_count(self):
        return self.spectro.data_count

    @data_count.write
    def data_count(self, value):
        self.spectro.data_count = value

    @attribute
    def data_transmit(self):
        return self.spectro.data_transmit

    @data_transmit.write
    def data_transmit(self, value):
        self.spectro.data_transmit = value

    @attribute(dtype=str)
    def capture_mode(self):
        mode = self.spectro.capture_mode
        return mode.name

    @capture_mode.write
    def capture_mode(self, value):
        isvalue = isname = False
        try:
            val = int(value)
            isvalue = True
        except ValueError:
            isname = True
            value = value.upper()

        for mode in HamaCaptureMode:
            if isvalue and mode.value == val:
                break
            if isname and mode.name == value:
                break
        else:
            raise BaseException(f"hama capture mode {value} not supported")

        self.spectro.capture_mode = mode

    @attribute(dtype=str)
    def hama_trigger_mode(self):
        mode = self.spectro.hama_trigger_mode
        return mode.name

    @hama_trigger_mode.write
    def hama_trigger_mode(self, value):
        isvalue = isname = False
        try:
            val = int(value)
            isvalue = True
        except ValueError:
            isname = True
            value = value.upper()

        for mode in HamaTriggerMode:
            if isvalue and mode.value == val:
                break
            if isname and mode.name == value:
                break
        else:
            raise BaseException(f"hama trigger mode {value} not supported")

        self.spectro.hama_trigger_mode = mode

    # END HamaSpectro specific attributes

    #  STANDARD Spectro attributes are implemented by coding
    #    functions called by the base DS implementation
    #
    # BEGIN implementation of functions required by BaseSpectroDS

    def get_spectrum_size(self):
        return self.spectro.spectrum_size

    def get_number_points(self):
        return self.spectro.number_frames

    def set_number_points(self, value):
        self.spectro.number_frames = value

    def get_acquired_points(self):
        return self.spectro.acquired_frames

    def get_model(self):
        return self.spectro.get_model()

    def get_info(self):
        return self.spectro.get_info()

    def get_preset_value(self):
        return self.spectro.preset_value

    def set_preset_value(self, value):
        self.spectro.preset_value = value

    def get_trigger_mode(self):
        return self.spectro.trigger_mode

    def set_trigger_mode(self, mode):
        self.spectro.trigger_mode = mode

    def get_readout_time(self):
        return self.spectro.readout_time

    def prepare_acquisition(self):
        self.spectro.prepare_acquisition()

    def start_acquisition(self):
        self.spectro.start_acquisition()

    def abort_acquisition(self):
        self.spectro.abort_acquisition()

    def stop_acquisition(self):
        self.spectro.stop_acquisition()

    # END implementation of functions required by BaseSpectroDS


def run_it():
    run((HamaSpectroDS,))

if __name__ == "__main__":
    run_it()
