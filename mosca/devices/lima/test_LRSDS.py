#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Python script to test lima spectro via TangoDS
"""

"""
usage example:

export TANGO_HOST='id16b:20000'


from test_ds import test_soft, dev
from test_ds import test_sync
from test_ds import dev
test_soft(dev, 5, 25)
test_sync(dev, 50)

"""


import os
import sys
import time

from tango import DeviceProxy
import tango

def trig_test(ds_url):

    try:
        dev = DeviceProxy(ds_url)
        dev.ping()
    except tango.DevFailed:
        print(f"Cannot import {ds_url} (DS not started ?)")
        sys.exit()

    print(f"DS State = {dev.State()}")
    points_count = 100
    # PREPARE
    dev.capture_mode = "Trigger"
    dev.trigger_mode = "Sync"
    dev.number_points = points_count
    dev.preset_value = 8
    # NB: preset_cycle is adjusted automatically

    # SET A ROI
    print("Add Aula counter 55 88")
    dev.addCounter(["Aula", "55", "88"])
    print(dev.getCounterInfo("Aula"))

    # START
    dev.startAcq()

    # POLL
    while dev.acq_nb_points < points_count:
        time.sleep(0.3)
        print(f"{dev.acq_nb_points}/{points_count}")

    print(f"Acquiered {dev.acq_nb_points}/{points_count}")

    # READ DATA
    data = dev.data
    data_length = len(data)

    print(f"len(data)={data_length} ({points_count * dev.spectrum_size})")
    print("data=", data)

    # READ ROI (calculated by DS)
    roi_data = dev.getCounterValue("Aula")
    print("roi values =", roi_data)

    # END
    print("end of test")




def main(args):
    try:
        TH = os.environ["TANGO_HOST"]
    except KeyError:
        print("Cannot found TANGO_HOST environment variable")
        sys.exit()

    if len(sys.argv) == 2:
        print(f"Running test on {sys.argv[1]} (TANGO_HOST={TH})")
        trig_test(sys.argv[1])
    else:
        print("usage:")
        print(f"    {sys.argv[0]} <tango_server_url>")


if __name__ == "__main__":
    main(sys.argv)
