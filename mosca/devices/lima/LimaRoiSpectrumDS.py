#
#
#
#
#

from tango.server import run, command, attribute, device_property

from mosca.BaseSpectroDS import BaseSpectroDS
from mosca.devices.lima.LimaRoiSpectrum import LimaRoiSpectrum


def print_lrsds(msg):
    print(f"    [LRSDS]:{msg}")

class LimaRoiSpectrumDS(BaseSpectroDS):

    default_detector_name = "limaRoi"

    # URL of the Tango Lima device.
    # ex: id42/roi2spectrum/zyla
    lima_roi_spectrum_url = device_property(dtype=str)

#    def __init__(self, *args, **kwargs):
#        print_lrsds("LimaRoiSpectrumDS: init_device()")
#        super().__init__(args, kwargs)

    def init_device(self):
        print_lrsds("LimaRoiSpectrumDS: init_device()")

        self.spectro = LimaRoiSpectrum()
        self.spectro.lima_url = self.lima_roi_spectrum_url

        # ?
        super().init_device()

        print_lrsds("LimaRoiSpectrumDS: ready")

    def always_executed_hook(self):
        print_lrsds("aeh")

#        if self.spectro.is_acquiring():
#            self.set_running()
#        else:
#            self.set_idle()
#
#    @command(dtype_in=int)
#    def setNumberModules(self, value):
#        self.spectro.number_modules = value
#
#    @attribute(dtype=int)
#    def spectrum_size(self):
#        return self.spectro.spectrum_size
#
#    @spectrum_size.write
#    def spectrum_size(self, value):
#        self.spectro.spectrum_size = value
#
#    @attribute(dtype=bool)
#    def add_random(self):
#        return self.spectro.add_random
#
#    @add_random.write
#    def add_random(self, value):
#        self.spectro.add_random = value
#
#    def get_number_channels(self):
#        return self.spectro.number_modules
#
#    def get_spectrum_size(self):
#        return self.spectro.spectrum_size
#
#    def get_number_points(self):
#        return self.spectro.number_frames
#
#    def set_number_points(self, value):
#        self.spectro.number_frames = value
#
#    def get_acquired_points(self):
#        return self.spectro.acquired_frames
#
#    def get_model(self):
#        return self.spectro.get_model()
#
#    def get_info(self):
#        return self.spectro.get_info()
#
#    def get_preset_value(self):
#        return self.spectro.preset_value
#
#    def set_preset_value(self, value):
#        self.spectro.preset_value = value
#
#    def get_trigger_mode(self):
#        return self.spectro.trigger_mode
#
#    def set_trigger_mode(self, mode):
#        self.spectro.trigger_mode = mode
#
#    def get_readout_time(self):
#        return self.spectro.readout_time
#
#    def set_readout_time(self, value):
#        self.spectro.readout_time = value
#
#    def prepare_acquisition(self):
#        self.spectro.prepare_acquisition()
#
#    def start_acquisition(self):
#        self.spectro.start_acquisition()
#
#    def abort_acquisition(self):
#        self.spectro.abort_acquisition()
#
#    def stop_acquisition(self):
#        self.spectro.stop_acquisition()


def run_it():
    run((LimaRoiSpectrumDS,))

if __name__ == "__main__":
    run_it()
