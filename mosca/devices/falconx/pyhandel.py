  
import os
import cffi
import logging
import numpy as np
import time
import glob
import math
import threading
import struct

from enum import Enum

from mosca.bcu_logger import bcu_logger
from mosca.Spectrometer import SpectroDataHandler

# TO CHANGE THE INCLUDE FILE DEFINITIONS directory CHANGE the env variable HANDEL_INC_PATH
#   otherwise handel_constants.h and handel_errors.h are loaded from handel_inc under this
#   directory

# import constants from include file
import mosca.devices.falconx.handel_constants as HANDEL_C
import mosca.devices.falconx.handel_errors as HANDEL_E

# load error definitions from include file
from mosca.devices.falconx.handel_errors import HANDEL_ERRORS

CLOCK_TICK = 320e-9

MAP_MIN_PIX_BUFFER = 1

# SCA corrections
CORR_DT = 1       
CORR_LVT = 2


EDF_FILE, HDF5_FILE = (1,2)

HANDEL_LOGLEVEL = {
    'ERROR': 1,
    'WARNING': 2,
    'INFO': 3,
    'DEBUG': 4,
}

# errors produced by the logic in this module 
PY_HANDEL_ERROR_BASE=50000
PY_HANDEL_CANNOT_LIB, PY_HANDEL_WRONG_PATH, PY_CONFIG_NOTFOUND, \
      PY_ERROR_NO_MODULES, PY_WRONG_PRESET_VALUE, PY_WRONG_PRESET_MODE, \
      PY_WRONG_GATE_VALUE, PY_WRONG_DETECTOR_MODE, PY_SYNC_ONE_BOARD, \
      PY_SYNC_NOT_IMPLEMENTED, PY_CANNOT_START, PY_CANNOT_STOP, \
      PY_WRONG_SCA_DEFS, PY_NO_DSP_PARNAME, PY_WRONG_CHANNEL_NUM, \
      PY_BUFFER_OVERRUN, PY_NO_DATA, PY_WRONG_LOGLEVEL, PY_PRESET_REFRESH = \
         list(range(PY_HANDEL_ERROR_BASE, PY_HANDEL_ERROR_BASE+19))

PY_HANDEL_ERRORS = {
   PY_HANDEL_CANNOT_LIB: ("PY CANNOT LOAD HANDEL DLL", "Handel library cannot be loaded"),
   PY_HANDEL_WRONG_PATH: ("PY PATH DOES NOT EXIST", "Path does not exist"),
   PY_CONFIG_NOTFOUND:   ("PY CONFIGURATION FILE NOT FOUND", "Cannot find configuration file"),
   PY_ERROR_NO_MODULES:  ("PY NO MODULES FOUND", "Cannot detect any module"),
   PY_WRONG_PRESET_VALUE: ("PY WRONG PRESET VALUE", "Preset mode should larger than refresh_rate"),
   PY_WRONG_PRESET_MODE: ("PY WRONG PRESET MODE", "Preset mode should be in the range 1 to 4"),
   PY_WRONG_GATE_VALUE:  ("PY WRONG GATE VALUE", "Gate value should be either 1 (use)  or 0 (ignore)"),
   PY_WRONG_DETECTOR_MODE:    ("PY WRONG DETECTOR MODE", "Mode value should be either 0 (MCA MODE) or 1 (MAP MODE)"),
   PY_CANNOT_START:      ("PY CANNOT_START", "Cannot start"),
   PY_CANNOT_STOP:       ("PY CANNOT_STOP", "Cannot stop"),
   PY_SYNC_ONE_BOARD:    ("PY SYNC ONE BOARD IMPOSSIBLE", "Cannot use SYNC mapping mode with only one board"),
   PY_SYNC_NOT_IMPLEMENTED: ("PY SYNC MODE NOT IMPLEMENTED YET", "Cannot use SYNC mapping mode with this pyhandel version"),
   PY_WRONG_SCA_DEFS:    ("PY WRONG SCA DEFS","Wrong SCA definition parameters"),
   PY_NO_DSP_PARNAME:    ("PY NO DSP PARNAME", "Need param name for setting values. Set parname with get param first"),
   PY_WRONG_CHANNEL_NUM: ("PY WRONG CHANNEL NUM", "Wrong channel number"), 
   PY_BUFFER_OVERRUN:    ("PY BUFFER OVERRUN", "Buffer overrun"), 
   PY_NO_DATA:           ("PY NO DATA", "No data available"), 
   PY_WRONG_LOGLEVEL:    ("PY WRONG LOGLEVEL", "Wrong log level selected"), 
   PY_PRESET_REFRESH:    ("PY PRESET REFRESH", "preset cannot be shorter than refresh rate"), 
}

HANDEL_DEFAULT_ERROR = "UNKNOWN_ERROR_CODE", None

# XIA MODELS
MODEL_DXP4C, MODEL_DXP2X, MODEL_XMAP, \
   MODEL_MERCURY, MODEL_FALCONX = list(range(5))

XIA_MODELS = {
   "dxp4c2x":MODEL_DXP4C, 
   "dxp2x":MODEL_DXP2X, 
   "xmap": MODEL_XMAP, 
   "mercury": MODEL_MERCURY,
   "falconxn": MODEL_FALCONX,
}

# preset_modes
XIA_PRESET_MODES = {
    HANDEL_C.XIA_PRESET_NONE: "none",
    HANDEL_C.XIA_PRESET_FIXED_REAL: "realtime",
    HANDEL_C.XIA_PRESET_FIXED_LIVE: "livetime",
    HANDEL_C.XIA_PRESET_FIXED_EVENTS: "events",
    HANDEL_C.XIA_PRESET_FIXED_TRIGGERS: "triggers",
}

# values from Taco device server. SCA mode is never used anyway
class DetectorMode(Enum):
    MCA = 0x01
    SCA = 0x02
    MAP = 0x04

class MapMode(Enum):
    SYNC = 0x01
    GATE = 0x02

MAX_STRING_LENGTH=80

# mapping control mode
XIA_MAPPING_CTL_USER = 0.0
XIA_MAPPING_CTL_GATE = 1.0
XIA_MAPPING_CTL_SYNC = 2.0

# STATES
STATE_INIT, STATE_READY, STATE_RUNNING, STATE_UNKNOWN = range(4)

STOP_ATTEMPTS = 10

HANDEL_C_DECL = """
typedef unsigned char  byte_t;
typedef unsigned char  boolean_t;
typedef unsigned short parameter_t;
typedef unsigned short flag_t;

int xiaInit(char *iniFile);
int xiaInitHandel(void);
int xiaGetDetectorItem(char *alias, char *name, void *value);
int xiaGetNumDetectors(unsigned int *numDet);
int xiaGetDetectors(char *detectors[]);
int xiaGetDetectors_VB(unsigned int index, char *alias);
int xiaDetectorFromDetChan(int detChan, char *alias);
int xiaGetFirmwareItem(char *alias, unsigned short decimation, char *name, void *value);
int xiaGetNumFirmwareSets(unsigned int *numFirmware);
int xiaGetFirmwareSets(char *firmware[]);
int xiaGetFirmwareSets_VB(unsigned int index, char *alias);
int xiaGetNumPTRRs(char *alias, unsigned int *numPTRR);
int xiaGetModuleItem(char *alias, char *name, void *value);
int xiaGetNumModules(unsigned int *numModules);
int xiaGetModules(char *modules[]);
int xiaGetModules_VB(unsigned int index, char *alias);
int xiaModuleFromDetChan(int detChan, char *alias);
int xiaStartSystem(void);
int xiaEndSystem(void);
int xiaSetAcquisitionValues(int detChan, char *name, void *value);
int xiaGetAcquisitionValues(int detChan, char *name, void *value);
int xiaRemoveAcquisitionValues(int detChan, char *name);
int xiaGainOperation(int detChan, char *name, void *value);
int xiaGainCalibrate(int detChan, double deltaGain);
int xiaStartRun(int detChan, unsigned short resume);
int xiaStopRun(int detChan);
int xiaGetRunData(int detChan, char *name, void *value);
int xiaDoSpecialRun(int detChan, char *name, void *info);
int xiaGetSpecialRunData(int detChan, char *name, void *value);
int xiaLoadSystem(char *type, char *filename);
int xiaSaveSystem(char *type, char *filename);
int xiaBoardOperation(int detChan, char *name, void *value);
int xiaExit(void);
int xiaEnableLogOutput(void);
int xiaSuppressLogOutput(void);
int xiaSetLogLevel(int level);
int xiaSetLogOutput(char *fileName);
void xiaGetVersionInfo(int *rel, int *min, int *maj, char *pretty);
int xiaGetParameter(int detChan, const char *name, unsigned short *value);
int xiaGetNumParams(int detChan, unsigned short *numParams);
int xiaGetParamData(int detChan, char *name, void *value);
int xiaGetParamName(int detChan, unsigned short index, char *name);
"""

class DxpModule(object):
    def __init__(self, modtype, number, alias, modchannum):
        self.modtype = modtype
        self.number = number
        self.alias = alias
        self.channels = []
        self.master = -1
        self._modchannum = modchannum

    def add_channel(self, channel):
        self.channels.append(channel) 
        if self.master == -1:
            self.master = channel.numchan
    
    @property
    def number_of_channels(self):
        """ total number of channels declared for this module,
            taking into account channels with alias == -1
        """
        return self._modchannum
    
    @property
    def number_of_active_channels(self):
        """ number of active channels associated to this module,
            discarding channels with alias == -1
        """
        return len(self.channels)


class DxpChannel(object):
    def __init__(self, module, index_in_mod, numchan):
        self.module = module
        self.index_in_mod = index_in_mod
        self.numchan = numchan  # global channel number

    @property
    def num(self):
        return self.numchan

class XiaFile(object):

    def __init__(self):

        self._folder = None
        self._prefix = None
        self._numbers = [-1,]*3

        self._autosave = False
        self._bidirect = False

        self.headers = []

        self.file_format = EDF_FILE

    def set_values(self, folder=None, prefix=None, 
                         numbers=None):

        if folder is not None:
            self._folder = folder

        if prefix is not None:
            self.prefix = prefix

        if numbers is not None:
            self.numbers = numbers

    @property
    def folder(self):
        return self._folder

    @folder.setter
    def folder(self, value):
        if value is not None:
            self._folder = value

    @property
    def prefix(self):
        return self._prefix

    @prefix.setter
    def prefix(self, value):
        if value is not None:
            self._prefix = value

    @property
    def numbers(self):
        return self._numbers

    @numbers.setter
    def numbers(self, nums):
        if len(nums) == 3:
            self._numbers = nums
        else:
            raise BaseException("file numbers length is wrong. it should be 3")

    @property
    def autosave(self):
        return self._autosave

    @autosave.setter
    def autosave(self, value):
        self._autosave = value 

    @property
    def bidirect(self ):
        return self._bidirect 

    @bidirect.setter
    def bidirect(self, value):
        self._bidirect = value
       
    def reset_header(self):
        self.headers = []

    def add_header(self, header_str):
        self.headers.append(header_str)

    def header_str(self):
        return ";\n".join(self.headers)

    @property
    def file_template(self):
        nr = self.numbers
        filename = f"{self.prefix}_xiaNN_{nr[0]}_{nr[1]}_{nr[2]}.edf"
        return os.path.join(self.folder, filename)

class DummyLock(object):
    def __enter__(self):
        pass

    def __exit__(self,*args):
        pass

class HandelError(IOError):
    def __init__(self, errno, errstr=None, errdesc=None):
        self.errno = errno
        if errno in HANDEL_ERRORS:
            error_str, error_desc = HANDEL_ERRORS[errno]
        elif errno in PY_HANDEL_ERRORS:
            error_str, error_desc = PY_HANDEL_ERRORS[errno]
        else:
            error_str, error_desc = HANDEL_DEFAULT_ERROR
        self.strerror = errstr is None and error_str or errstr
        self.description = errdesc is None and error_desc or errdesc
        self.args = errno, error_str, error_desc

    def __str__(self):
        s = "[HandelError {}] {}".format(self.errno, self.strerror)
        if self.description:
            s += ": {}".format(self.description)
        return s

class Handel(object):

    product = "XIA - FalconX"
    thread_lock = threading.Lock()

    def __init__(self, config_file="config.ini", lib_name="handel.dll", config_path=None, lib_path=None):
        self.xia_lib_loaded = False
        self.xia_loaded_config = None
        self.info_loaded = False
        self.loading = False

        self.libpath = None
        self.libname = None
        self.lib = None

        self.config_path = None
        self.config_filename = None
        
        self.log = bcu_logger('pyhandel')

        print(f'Handel INIT: lib_path: {lib_path} lib_name: {lib_name}')
        self.set_lib_path(lib_path, lib_name)
        self.set_config_path(config_path, config_file)

        self.data_handler = SpectroDataHandler()
        self.data_handler.multichannel = True 

        self.init_defaults()

        self._lock = DummyLock()
        self.auto_update = False

        self.log.debug("Handel module init")

    def init_defaults(self):

        self.state = STATE_INIT
        self.last_update = 0
        self.started = False

        self.modules = []
        self.channels = []  

        self.gate_master = 0
        self.sync_master = 1
        self.sync_ignore = 1 

        self.input_polarity = 0

        self.gate_ignore = -1
        self.clear = 1
        self.clear_next = False

        self.spectrum_length = -1
        self.baseline_length = -1

        self.map_nb_pixels = 1
        self.map_nb_pixels_buffer = 1
        self.map_sync = 0
        self.map_buffers = "ab"

        self.file = XiaFile()

        self.default_preset_mode = "realtime"
        self.default_preset_value = 1000 

        self.preset_mode = None
        self.preset_value = self.default_preset_value
        self.preset_set = False

        self.preset_time = 1000   # keeps latest sent preset time - 
                                  # to be used in case preset mode is changed to some time mode
        self.preset_counts = 100  # keeps latest preset count
                                  # to be used in case preset mode is changed to some count mode

        self.xia_model = None
        self.system_started = False

        self.map_current_pixel = 0
        self.map_read_pixel = 0
        self.map_file_saved = False
        self.map_buffer_index = 0

        self.metadata_labels = ['chnum', 'output', 'icr', 'ocr', 
                'livetime', 'deadtime', 'realtime', 'triggers', 'livetime_events', 'deadtime_correction' ]

        self.map_raw_stats_labels = ['realtime', 'livetime', 
                                     'triggers', 'outputs', 'chno']
        self.t1 = None

        self.detector_mode = DetectorMode.MCA
        self.data_handler.set_metadata_labels(self.metadata_labels)

    def set_lib_path(self, path, libname="handel.dll"):
        if path is not None:
            libpath = os.path.abspath(path)
            if self.check_isdir(libpath):
                self.libpath = libpath
            else:
                raise HandelError(PY_HANDEL_WRONG_PATH, 
                                  errdesc="{} does not exist".format(libpath))
  
        self.libname = libname

    def get_info(self):
        lib_loaded = self.xia_lib_loaded and "Loaded" or "Not loaded"  
        config_loaded = self.info_loaded and "Loaded" or "Not loaded"  
        config = f"config {self.config_filename} at {self.config_path} ({config_loaded})"

        info = f"{self.product} - lib {self.libname} at {self.libpath}" \
               f" ({lib_loaded}). " \
               f"{config}."
    
        return info

    def set_config_path(self, path, config_file=None):
        if path is not None:
            config_path = os.path.abspath(path)
            if self.check_isdir(config_path):
                self.config_path = config_path
            else:
                raise HandelError(PY_HANDEL_WRONG_PATH, 
                           errdesc="{} does not exist".format(config_path))

        self.config_filename = config_file

    def get_config_path(self):
        return self.config_path

    def get_state(self):
        if self.auto_update:
            return STATE_RUNNING

        return self._update_state()

    def _update_state(self):

        state = self.state

        if self.state == STATE_RUNNING:
            if self.run_started:
                self.update_data(running=True)

                if not self.xia_is_running():
                    state = STATE_READY

            if self.state != STATE_RUNNING:
                state = STATE_READY

        if state != self.state:
            self.state = state

        return self.state

    def get_map_state(self):
        stat = self.get_state()
        saving_done = self.data_handler.is_saving_done()

        if saving_done:
            self.map_file_saved = True
        else:
            self.map_file_saved = False

        if stat == STATE_READY:
            state = 0
        elif stat == STATE_RUNNING:
            state = 1
        elif not saving_done:
            state = 2
        else:
            state = -1
        
        return [state, 
                self.map_current_pixel, 
                self.map_read_pixel, 
                self.map_file_saved]

    def needs_update(self):
        if not self.started:
             return False

        if  (time.time() - self.last_update) < self.refresh_rate:
             return False
        else:
             return True

    def update_data(self, forced=False, running=True):
        if forced or self.needs_update(): 
            if self.detector_mode == DetectorMode.MAP: 
                self.update_map_data()
                if forced:  
                    # at the end of the acquisition. 
                    # do it once more. in case both buffers are full
                    self.update_map_data()

            if self.detector_mode == DetectorMode.MCA: 
                self.update_mca_data(running=running)

            self.last_update = time.time()

    def update_mca_data(self, running=True):
        data = self.read_spectra_data()
        stats = self.read_spectra_stat()
        stats = stats.reshape((self.num_chans, len(self.metadata_labels)))
        self.data_handler.add_spectra(data, metadata=stats, completed=(not running))

    def update_map_data(self):

        # VR avoid access
        # curr_pixels = [ self.get_current_pixel(c.num) for c in self.channels ]
        # self.map_current_pixel = min( curr_pixels )
        curr_pixels = self.get_current_pixel(0)
        self.map_current_pixel = curr_pixels

        if self.time_first is None and self.map_current_pixel != 0:
             self.time_first = time.perf_counter()

        # VR avoid access
        #for no, cpix in enumerate(curr_pixels):
        #    if cpix != self.map_current_pixel:
        #        pass

        # check if any channel overruns
        # VR avoid access
        #if any( [ self.is_buffer_overrun(c.num)  for c in self.channels ] ):
        #    raise HandelError(PY_BUFFER_OVERRUN)
        
        # read and switch buffers all at the same time (when all full)
        
        # read and switch buffers all at the same time (when all full)
        for c in self.channels:
           if not self.is_buffer_full(c.num, self.map_buffer_index):
               return

        #if all([self.is_buffer_full(c.num, self.map_buffer_index) 
        #                  for c in self.channels ]):
        t0 = time.perf_counter()
        for chanindex, chan in enumerate(self.channels):
            self.read_map_buffer(chan.num, chanindex)
            self.set_buffer_done(chan.num, self.map_buffer_index)
        self.elapsed_read = time.perf_counter()-t0

        self.parse_map_buffers()
        self.map_buffer_index = (self.map_buffer_index == 0 ) and 1 or 0

    def get_config_filename(self):
        return self.config_filename
      
    def get_config_filelist(self):
        filelist = glob.glob(os.path.join(self.config_path,"*ini"))
        filelist = [os.path.basename(fname) for fname in filelist]
        return filelist


    def check_isdir(self, path):
        if not os.path.exists(path) or not os.path.isdir(path):
            return False
        return True

    def get_file_parameters(self):
        # TODO
        flags = (self.file.autosave and 0x1) | (self.file.bidirect and 0x2)
        retvals = [self.file.folder, self.file.prefix, self.file.numbers, flags]
        return retvals

    def set_file_header(self, headers):
        # TODO
        for head_str in headers:
            self.file.add_header(head_str)

    def get_file_folder(self):
        return self.file.folder
    def set_file_folder(self,value):
        self.file.folder = value

    def get_file_prefix(self):
        return self.file.prefix
    def set_file_prefix(self,value):
        self.file.prefix = value

    def get_file_numbers(self):
        return self.file.numbers
    def set_file_numbers(self,numbers):
        self.file.numbers = numbers

    def get_file_autosave(self):
        return (self.file.autosave is True) and 1 or 0
    def set_file_autosave(self, value):
        self.file.autosave = value and True or False

    def get_file_bidirect(self):
        return (self.file.bidirect is True) and 1 or 0
    def set_file_bidirect(self,value):
        self.file.bidirect = value and True or False

    def get_file_template(self):
        return self.file.file_template

    # decorator func to protect with a mutex lock
    def liblock(func):
        def inner(*args,**kwargs):
            self = args[0]
            if not self.xia_loaded_config:
                if not self.loading:
                   self.xia_init()
            with Handel.thread_lock:
                 return func(*args,**kwargs)
        return inner

    def xia_init(self):
        self.load_config(forcelib=True)

    def load_config(self, filename=None, forcelib=False):
        self.loading = True

        if not self.xia_lib_loaded or forcelib:
            self.load_handel_lib()

        if filename is not None:
             config_filename = filename
        else:
             config_filename = self.config_filename

        if not os.path.exists(config_filename):
            if self.config_path is not None:
                config_file = os.path.join(self.config_path, config_filename)
            else:
                config_file = os.path.abspath(config_filename)
        else:
            config_file = os.path.abspath(config_filename)

        if not os.path.exists(config_file):
            raise HandelError(PY_CONFIG_NOTFOUND, 
                  errdesc="config file ({}) cannot be found".\
                       format(config_file))

        self.log.debug("Starting config loading")

        if self.xia_loaded_config: 
            self.exit_handel() 

        self.load_system(config_file)

        try:
            if self.system_started:
                self.end_system()
            self.start_system()
        except HandelError as e:
            self.log.warning(str(e))
            

        self.xia_loaded_config = config_file
        self.config_filename = config_filename

        self.reload_all()
        self.log.debug("Finish config loading")

        self.info_loaded = True
        self.loading = False
        self.state = STATE_READY

        self.preset_set = False

        self.data_handler.add_header('xcfg', config_file, keep=True)

    def reload_all(self):
        self.info_loaded = False

        self.init_defaults()
        self.load_defaults()

    def load_defaults(self):
        num_modules = self.get_num_modules()

        if num_modules == 0:
             raise HandelError(PY_ERROR_NO_MODULES)

        # load modules and channel info
        for modno in range(num_modules):

            modalias = self.get_module_alias(modno)
            modtype = self.get_module_item(modalias,"module_type")
            if self.xia_model is None:
                if modtype in XIA_MODELS:
                    self.xia_model = XIA_MODELS[modtype]
                else:
                    self.xia_model = MODEL_XMAP

            # get number of channels declared for this module
            # it takes into account disabled channels with alias == -1
            modchannum = self.get_module_item(modalias, "number_of_channels", rtype=int)
            module = DxpModule(modtype, modno, modalias, modchannum)

            for channo_inmod in range(modchannum):
                channel_alias = self.get_module_item(modalias, f"channel{channo_inmod}_alias", rtype=int)
                if channel_alias != -1: # disabled channel has channel_alias == -1 
                    chan = DxpChannel(modno, channo_inmod, channel_alias)
                    module.add_channel(chan)
                    self.channels.append(chan)

            self.modules.append(module)
        
        self.data_handler.set_number_channels(len(self.channels))
        # load_spectrum_defs
        chan = self.channels[0].num

        mca_bin_width = self.get_acq_value(chan, "mca_bin_width")
        spectrum_length = self.get_spectrum_length(chan)

        self.spectrum_bin_width = mca_bin_width
        self.spectrum_length = spectrum_length
        self.spectrum_min = 0
        self.spectrum_max = self.spectrum_min + self.spectrum_length -1

        try:
            if self.xia_model not in [MODEL_FALCONX]:  
                baseline_length = self.get_baseline_length(chan)
                self.baseline_length = baseline_length
        except HandelError as e:
            self.log.warning(str(e))

        # init master channel and polarity
        # set gate master (if it is not MERCURY)
        self.init_master()

    def xia_start(self):

        self.state = STATE_RUNNING

        self.last_update = 0
        self.started = False
        self.run_started = False
        self.time_first = None

        print(f'\n<START>')
        self.xia_stop()

        if self.clear or self.clear_next:
             resume = False
        else:
             resume = True

        self.set_detector_mode() # initialize detector mode with current one.
                                 # by default MCA
        if  self.xia_model == MODEL_FALCONX:
            self.refresh_rate = self.get_refresh_rate()
        else:
            self.refresh_rate = 0.02

        self.num_chans = len(self.channels)

        self.data_handler.init_data(self.spectrum_length, self.num_chans)
        self.data_handler.set_metadata_labels(self.metadata_labels)

        if self.detector_mode == DetectorMode.MAP:
            # init map variables and data
            self.map_current_pixel = 0
            self.map_read_pixel = 0
            self.map_file_saved = False
            self.map_buffer_index = 0

        else:
            if not self.preset_set:
                if self.preset_mode is not None:
                    self.set_preset_mode(self.preset_mode)
                self.set_preset_value(self.default_preset_value) 

            preset_mode = self.get_preset_mode()
            if preset_mode in (HANDEL_C.XIA_PRESET_FIXED_REAL,
                               HANDEL_C.XIA_PRESET_FIXED_LIVE):
                if self.preset_value <= self.refresh_rate:
                   raise HandelError(PY_PRESET_REFRESH, errdesc=f"preset {self.preset_value} smaller than refresh {self.refresh_rate}")

        self.apply_acq_values()
        self.start_update_loop()

        t0 = time.perf_counter()
        self.start_run()
        elapsed = time.perf_counter() -t0
        print(f'   -     total elapsed to start {elapsed:3.3f} secs')
        self.clear_next = False

    def xia_stop(self):


        #if not self.xia_is_running():
        #    print(f' xia stopped - but it is not running. nothing to do')
        #    return
            
        self.do_stop = True

        # allow for thread to stop
        start_stopping = time.perf_counter()
        while self.auto_update:
            # if an update loop is running
            # the loop will call stop_run() on exit
            time.sleep(0.1)
            if time.perf_counter() - start_stopping > 1:
                print(f'TIMEOUT stopping pyhandel update loop')
        
        # self.stop_run()

        if not self.wait_idle():
            print(f'timeout waiting for idle state')
            raise HandelError(PY_CANNOT_STOP)

        if self.auto_update:
            if self.t1 is not None and self.t1.is_alive():
                print(f'loop is still running')
                raise HandelError(PY_CANNOT_STOP)
            else:
                self.auto_update = False

    def xia_is_running(self):
        for chan in self.channels:
            if self.get_channel_running(chan.num):
                return True
        return False

    def wait_idle(self, timeout=2):
        start_wait = time.perf_counter()
        while self.xia_is_running():
            if time.perf_counter()-start_wait > timeout:
                break
            time.sleep(0.1)
        return not self.xia_is_running()

    def wait_running(self, timeout=0.3):
        start_wait = time.perf_counter()
        while True:
            is_running = self.xia_is_running()
            if not is_running and time.perf_counter()-start_wait < timeout:
                time.sleep(0.05)
                continue
            break
        return is_running

    def get_channel_pars(self):
        pars = [
            self.xia_model,
            len(self.modules), 
            len(self.channels),
            self.spectrum_length,
            self.spectrum_min,
            self.spectrum_max,
            self.baseline_length
        ]
        return pars

    def get_model(self):
        return self.xia_model

    def get_channel_defs(self):
        return [chan.num for chan in self.channels]

    def set_clear_next(self, value=True):
        value = (value and True) or False
        self.clear_next = value 

    def get_clear_next(self):
        return self.clear_next

    def get_acq_pars(self):
        if self.detector_mode == DetectorMode.MCA:
             det_mode = DetectorMode.MCA.value
             gate_ignore = self.get_gate_ignore()
             if gate_ignore: 
                 gate = 0 
             else:
                 gate = 1 
             return [det_mode,
                     gate,
                     self.clear,
                     self.preset_mode]
        else:  # MAP
             det_mode = DetectorMode.MCA.value | DetectorMode.MAP.value
             return [det_mode,
                     self.map_sync, 
                     self.map_nb_pixels,
                     self.map_nb_pixels_buffer]

    def get_detector_mode(self):
        return self.detector_mode
    
    def set_detector_mode(self, mode=None, gate_ignore=None):
        if mode is None:
            mode = self.detector_mode

        if mode is None:   # no mode selected
            raise HandelError(PY_WRONG_DETECTOR_MODE)

        if mode in (DetectorMode.MCA, DetectorMode.MCA.value):
            self.set_mca_mode()
            self.detector_mode = DetectorMode.MCA
        elif mode in (DetectorMode.MAP, DetectorMode.MAP.value):
            self.set_map_mode(gate_ignore)
            self.detector_mode = DetectorMode.MAP
        else:
            raise HandelError(PY_WRONG_DETECTOR_MODE)

        self.init_master()
        self.apply_acq_values()

        return self.detector_mode

    def init_master(self):
        if self.xia_model not in [MODEL_MERCURY, MODEL_FALCONX]:  
            for chan in self.channels:
                self.set_acq_value(chan, "gate_master", 1.0)

        polar_value = self.input_polarity and 1.0 or 0.0
        self.set_acq_value("all", "input_logic_polarity", polar_value)

        if self.preset_mode is None:
            self.set_preset_mode(self.default_preset_mode)

        self.sync_ignore = 1
        if len(self.modules) > 1:
            self.sync_ignore = 0 

    def get_refresh_rate(self):
        return self.get_acq_value(0, "mca_refresh")

    def set_refresh_rate(self, value):
        self.set_acq_value("all", "mca_refresh", float(value))

    def set_mca_mode(self):
        self.set_acq_value("all", "mapping_mode", 0.0)

        if self.gate_ignore == -1:
            self.set_acq_value("all", "gate_ignore", 1)
        else:
            self.set_acq_value("all", "gate_ignore", self.gate_ignore)

        self.apply_acq_values()

    def set_map_mode(self, gate_ignore=None):
        if gate_ignore is None:
            gate_ignore = self.get_gate_ignore()

        self.set_acq_value("all", "mapping_mode", 1.0)

        if gate_ignore == 1:
            #  SYNC MODE
            print(f'Setting sync mode')
            # self.set_acq_value("all", "pixel_advance_mode", XIA_MAPPING_CTL_SYNC)
            self.set_acq_value("all", "pixel_advance_mode", XIA_MAPPING_CTL_GATE)
            self.set_acq_value("all", "gate_ignore", 1)
        else:
            #  GATE MODE
            print(f'Setting gate mode')
            self.set_acq_value("all", "gate_ignore", 0)
            self.set_acq_value("all", "pixel_advance_mode", XIA_MAPPING_CTL_GATE)

    def get_gate_mode(self):
        if self.get_gate_ignore():
            return MapMode.SYNC
        else:
            return MapMode.GATE

    def set_gate_mode(self):
        self.set_detector_mode(mode=DetectorMode.MAP, gate_ignore=0)

    def set_sync_mode(self, sync=1):
        self.set_detector_mode(mode=DetectorMode.MAP, gate_ignore=1)

    def set_mca_acq_pars(self, gate, clear, preset_mode):
        """
        Acquisition parameters for non mapping mode
        """
        do_apply = False

        if gate not in [0,1]:
             raise HandelError(PY_WRONG_GATE_VALUE)

        self.set_mca_mode()

        do_apply = True

        self.clear = clear and 1 or 0

        if preset_mode != self.preset_mode:
            if preset_mode < 0 or preset_mode > 5:
                raise HandelError(PY_WRONG_PRESET_MODE)

            self.set_acq_value("all", "preset_type", preset_mode)
            self.preset_mode = preset_mode
            do_apply = True

        if do_apply:
            # call once per module 
            self.apply_acq_values()

    def set_map_acq_pars(self, sync, npixels, no_pix_buffer):
        if sync:
            raise HandelError(PY_SYNC_NOT_IMPLEMENTED)
            if self.sync_ignore:
                raise HandelError(PY_SYNC_ONE_BOARD)

        # gate or sync trigger modes are set in this case by
        # respecting the value already programmed.
        #
        # pixel advance mode -sync- is not implemented for now
        #  sync parameter is ignored
        self.set_map_mode()

        # nb of pixels / pixels per buffer
        print(f'   - setting map acq pars {sync} {npixels} {no_pix_buffer}')
        self.set_map_nb_pixels(npixels, do_apply=False)
        self.set_map_pixels_per_buffer(no_pix_buffer, do_apply=False)

        # apply
        self.apply_acq_values()
        self.init_map_buffer_data()

    def init_map_buffer_data(self):
        # allocate space in buffer
        buffer_length = self.get_buffer_length(0)

        nb_chans = len(self.channels)

        self.map_raw_buffer = [np.zeros(buffer_length, dtype=np.uint32) 
                                  for ch in range(nb_chans)]

        #  reserving buffer data space
        self.map_buf_data = np.zeros( \
                  (nb_chans, self.map_nb_pixels_buffer, self.spectrum_length), \
                  dtype=np.uint32) 

        nb_stats = len(self.metadata_labels)
        self.map_buf_stats = np.zeros(\
                  (nb_chans, self.map_nb_pixels_buffer, nb_stats), \
                  dtype=np.double)

        nb_raw_stats = len(self.map_raw_stats_labels)
        self.map_raw_stats = np.zeros(\
                  (nb_chans, self.map_nb_pixels_buffer, nb_raw_stats), \
                  dtype=np.uint32) 

    def get_map_pixel_mode(self):
        if self.detector_mode == DetectorMode.MCA:
            return "inactive"

        map_mode = self.get_acq_value(0,"pixel_advance_mode")
        if map_mode == XIA_MAPPING_CTL_GATE: 
            return "gate"
        elif map_mode == XIA_MAPPING_CTL_SYNC: 
            return "sync"
        elif map_mode == XIA_MAPPING_CTL_USER: 
            return "user"
        else:
            return "unknown"

    def get_map_nb_pixels(self):
        return self.map_nb_pixels

    def set_map_nb_pixels(self, npixels, do_apply=True):
        print(f'\n   - setting map nb pixels to {npixels}')
        self.set_acq_value("all", "num_map_pixels", npixels)
        self.map_nb_pixels = npixels

        if do_apply:
            self.apply_acq_values()
            self.init_map_buffer_data()

    def get_map_pixels_per_buffer(self):
        return self.map_nb_pixels_buffer

    def set_map_pixels_per_buffer(self, no_pix_buffer, do_apply=True):
        max_nb_buffer = int( (1024*1024 - 256) / (self.spectrum_length * 4 + 256) )
        #value = min(max_nb_buffer, no_pix_buffer)
        value = no_pix_buffer

        value = max(value, MAP_MIN_PIX_BUFFER)

        print(f'   - setting map nb pixels per buffer to {no_pix_buffer}')
        self.set_acq_value("all", "num_map_pixels_per_buffer", value)
        self.map_nb_pixels_buffer = value

        if do_apply:
            self.apply_acq_values()
            self.init_map_buffer_data()

    def get_map_current_pixel(self):
        return self.map_current_pixel

    def get_map_read_pixel(self):
        return self.map_read_pixel

    def is_map_file_saved(self):
        return self.map_file_saved

    # handel dll access
    def load_handel_lib(self):
        if self.xia_lib_loaded:
            return

        self.log.info("loading handel lib")
        self.xia_loaded_config = None

        # add libpath to PATH variable if any and libname in PATH

        if self.libpath is not None:
            p1 = os.path.join(self.libpath, self.libname)
            if os.path.exists(p1):
                os.environ['PATH'] = ";".join([os.environ['PATH'],
                                               self.libpath]) 
            else:
                p2 = os.path.join(self.libpath, "lib", self.libname)
                if os.path.exists(p2):
                    os.environ['PATH'] = ";".join([os.environ['PATH'],
                                                   os.path.join(self.libpath,
                                                                'lib')])
                else:
                    ermsg = "path provided for handel does not contain {} DLL.".\
                                             format(self.libname) 
                    print(f'{self.libpath}')
                    print(f'self.libname {ermsg}')
                    raise HandelError(PY_HANDEL_CANNOT_LIB, errdesc=ermsg)
                
        self.ffi = cffi.FFI()
        self.ffi.cdef( HANDEL_C_DECL )

        try:
            self.log.info("libname path is {0}".format(self.libname))
            self.lib = self.ffi.dlopen(self.libname)
            self.lib.xiaInitHandel() 
            self.xia_lib_loaded = True
            self.log.debug("lib loading done.")
        except Exception:
            emsg = "cannot load handel library"
            raise HandelError(PY_HANDEL_CANNOT_LIB, errdesc=emsg)
            self.lib = None

    @liblock
    def load_system(self, config_file):
        ret = self.lib.xiaInit(to_bytes(config_file))
        # ret = self.lib.xiaLoadSystem(b"handel_ini", to_bytes(config_file))
        check_ret(ret)

    @liblock
    def start_system(self):
        ret = self.lib.xiaStartSystem()
        check_ret(ret)
        self.system_started = True

    @liblock
    def exit_handel(self):
        ret = self.lib.xiaExit()
        check_ret(ret)

    @liblock
    def end_system(self):
        ret = self.lib.xiaEndSystem()
        check_ret(ret)

    @liblock
    def get_firmware_aliases(self):
        num = self.ffi.new("unsigned int *")
        ret = self.lib.xiaGetNumFirmwareSets(num)
        check_ret(ret)

        aliases = []

        for no in range(num[0]):
            arg = self.ffi.new("char []", MAX_STRING_LENGTH)
            ret = self.lib.xiaGetFirmwareSets_VB(no, arg)
            check_ret(ret)
            alias = self.ffi.string(arg)

            num = self.ffi.new("unsigned int *")
            ret = self.lib.xiaGetNumPTRRs(alias, num)
            num_ptrr = 0

            if HANDEL_ERRORS[ret][0] != 'XIA_LOOKING_PTRR':
                check_ret(ret)
                is_fdd = False
                num_ptrr = num[0]
            else:
                is_fdd = True
            
            if is_fdd:
                arg = self.ffi.new("char *")
                ret = self.lib.xiaGetFirmwareItem(alias, 0, b'filename', arg)
                check_ret(ret)
                filename = self.ffi.string(arg)
                info = filename
            else: 
                info=f'{num_ptrr}_ptrr'
                
            alias = alias.decode()
            aliases.append(f"{alias}:{info}")
          
        return aliases

    @liblock
    def get_num_detectors(self):
        """ Return the number of detectors configured
        """
        num = self.ffi.new("unsigned int *")
        ret = self.lib.xiaGetNumDetectors(num)
        check_ret(ret)

        return num[0]

    @liblock
    def get_num_modules(self):
        if self.info_loaded: 
            self.log.debug("cached value")
            return len(self.modules)

        num = self.ffi.new("unsigned int *")
        ret = self.lib.xiaGetNumModules(num)
        check_ret(ret)
        return num[0]

    @liblock
    def get_module_alias(self, modno):
        arg = self.ffi.new("char []", MAX_STRING_LENGTH)
        ret = self.lib.xiaGetModules_VB(modno, arg)
        check_ret(ret)
        alias = self.ffi.string(arg).decode()
        return alias

    @liblock
    def get_module_item(self, modalias, item, rtype=str):
        if rtype == str:
            value = self.ffi.new("char []", MAX_STRING_LENGTH)
        elif rtype == int:
            value = self.ffi.new("int *")

        ret = self.lib.xiaGetModuleItem(to_bytes(modalias),
                                        to_bytes(item),
                                        value)
        check_ret(ret)

        if rtype == str:
            value = self.ffi.string(value).decode()
        else:
            value = value[0]
        return value

    @liblock
    def get_acq_value(self, channel, item):
        value = self.ffi.new("double *")
        ret = self.lib.xiaGetAcquisitionValues(channel, to_bytes(item), value)
        check_ret(ret)
        return value[0]

    @liblock
    def set_acq_value(self, channel, item, value):
        if channel == "all":
            channel = -1

        self.log.debug("setting acq value ({}) to {}".format(item,value))

        item = to_bytes(item)
        ptr = self.ffi.new("double *", value)
        ret = self.lib.xiaSetAcquisitionValues(channel, item, ptr)
        check_ret(ret)

    @liblock
    def apply_acq_values(self):
        # one per module ?
        self.log.debug("applying acquisition values")

        dummy = self.ffi.new("double *")

        for mod in self.modules:
            ret = self.lib.xiaBoardOperation(mod.master, b'apply', dummy)
            check_ret(ret)

    def set_preset_value(self, value):
        # value for realt and livet in msec / programmed in secs

             # if not self.preset_set:
                 # # if preset_mode is not set. set realtime by default
                 # self.set_acq_value("all", "preset_value", self.default_preset_value)
                 # value = self.default_preset_value
                 # self.set_preset_mode(self.default_preset_mode)
             # else:
                 # raise HandelError(PY_WRONG_PRESET_MODE)

        if self.preset_mode is None:
            self.set_preset_mode(self.default_preset_mode)

        self.log.debug("checking preset mode %s" % self.preset_mode)

        if self.preset_mode in (HANDEL_C.XIA_PRESET_FIXED_REAL,
                                HANDEL_C.XIA_PRESET_FIXED_LIVE):
             self.mode_is_time = True
             dvalue = value*0.001
             print(f"   - is time. sending {dvalue}")
             self.preset_time = dvalue
             if self.xia_model == MODEL_FALCONX:
                 refresh_rate = self.get_refresh_rate()
                 if self.get_detector_mode() == DetectorMode.MCA and refresh_rate > dvalue:
                     raise HandelError(PY_WRONG_PRESET_VALUE)
        else:
             self.mode_is_time = False
             dvalue = value
             print(f"   - is not time. sending {dvalue}")
             self.preset_counts = dvalue

        self.preset_set = True
      
        if dvalue and dvalue != self.preset_value:
             self.set_acq_value("all", "preset_value", dvalue)
             self.preset_value = dvalue
             self.apply_acq_values()

    def get_preset_value(self):
        if self.preset_mode in (HANDEL_C.XIA_PRESET_FIXED_REAL,
                                HANDEL_C.XIA_PRESET_FIXED_LIVE):
            return self.preset_value*1000

        return self.preset_value

    def set_gate_ignore(self, value):
        if value:
            gate_ignore = 1.0
        else:
            gate_ignore = 0.0

        if gate_ignore != self.gate_ignore:
            self.set_acq_value("all", "gate_ignore", gate_ignore)
            self.gate_ignore = int(gate_ignore)
            do_apply = True
        else:
            do_apply = False

        if do_apply:
            self.apply_acq_values()

    def get_gate_ignore(self):
        return self.get_acq_value(0,"gate_ignore")
      
    def set_preset_mode(self, value=None, doapply=False):
        # both string and numerical values are accepted

        if value is None:
            value = "realtime" 

        for dvalue, strvalue in XIA_PRESET_MODES.items():
            if strvalue == value or dvalue == value:
                self.log.debug("setting preset mode to {}".format(strvalue))
                break  

            # accept also numeric value
            try:
               if dvalue == int(value):
                   break
            except ValueError:
               pass
        else:
            raise HandelError(PY_WRONG_PRESET_MODE)

        if dvalue == HANDEL_C.XIA_PRESET_FIXED_LIVE and \
            self.xia_model == MODEL_FALCONX:
                raise HandelError(HANDEL_E.XIA_UNIMPLEMENTED, 
                       errdesc="livetime mode not implemented for falconx")

        self.set_acq_value("all", "preset_type", dvalue)

        # check if programming previous time or counts preset value
        new_preset_value = None
        if dvalue != self.preset_mode:
            if self.is_time_mode(dvalue) and \
                     not self.is_time_mode(self.preset_mode):
                 new_preset_value = self.preset_time 
            elif not self.is_time_mode(dvalue) and \
                     self.is_time_mode(self.preset_mode):
                 new_preset_value = self.preset_counts

        if new_preset_value is not None:
            self.set_acq_value("all", "preset_value", new_preset_value)
            self.preset_value = new_preset_value

        self.preset_mode = dvalue

        if doapply:
            self.apply_acq_values()

    def is_time_mode(self, preset_mode):
        if preset_mode in [HANDEL_C.XIA_PRESET_FIXED_LIVE,
                           HANDEL_C.XIA_PRESET_FIXED_REAL]: 
            return True
        return False

    def get_preset_mode(self, as_string=False):
        dvalue = self.get_acq_value(0, "preset_type" )
        if as_string:
            return XIA_PRESET_MODES[dvalue]

        return dvalue

    @liblock
    def get_buffer_length(self, channel):
        value = self.ffi.new("unsigned long *")
        ret = self.lib.xiaGetRunData(channel, b'buffer_len', value)
        check_ret(ret)
        return value[0]

    @liblock
    def get_channel_running(self, channel):
        value = self.ffi.new("short *")
        ret = self.lib.xiaGetRunData(channel, b'run_active', value)
        check_ret(ret)
        return value[0] & 0x1

    @liblock
    def get_spectrum_length(self, channel=0):
        value = self.ffi.new("unsigned long *")
        ret = self.lib.xiaGetRunData(channel, b'mca_length', value)
        check_ret(ret)
        return value[0]

    def get_number_channels(self):
        return len(self.channels)
    
    def get_channels_module_and_index(self):
        return [f"{chan.module}:{chan.index_in_mod}" for chan in self.channels]

    def get_channels_alias(self):
        return [chan.numchan for chan in self.channels]

    @liblock
    def get_channel_realtime(self, channel):
        value = self.ffi.new("double *")
        ret = self.lib.xiaGetRunData(channel, b'realtime', value)
        check_ret(ret)
        return value[0]

    def get_baseline_length(self, channel):
        value = self.ffi.new("unsigned long *")
        ret = self.lib.xiaGetRunData(channel, b"baseline_length", value)
        check_ret(ret)
        return value[0]

    @liblock
    def get_baseline(self, channel):
        length = self.get_baseline_length(channel)
        array = np.zeros(length, dtype="uint32")
        data = self.ffi.cast("uint32_t *", array.ctypes.data)
        ret = self.lib.xiaGetRunData(channel, b"baseline", data)
        check_ret(ret)
        return array

    @liblock
    def get_param_value(self, channel, parname):
        value = self.ffi.new("unsigned short *")
        ret = self.lib.xiaGetParameter(channel, to_bytes(parname), value)
        check_ret(ret)
        return value[0]

    @liblock
    def set_param_value(self, channel, parname, value):
        ret = self.lib.xiaGetParameter(channel, to_bytes(parname), value)
        check_ret(ret)

    @liblock
    def get_param_nb(self):
        value = self.ffi.new("unsigned short *")
        ret = self.lib.xiaGetNumParams(self.channels[0].num, value)
        check_ret(ret)
        return value[0]

    @liblock
    def get_param_name(self, idx):
        arg = self.ffi.new("char []", MAX_STRING_LENGTH)
        ret = self.lib.xiaGetParamName(self.channels[0].num, idx, arg)
        check_ret(ret)
        name = self.ffi.string(arg).decode()
        return name

    @liblock
    def get_param_value(self, parname):
        value = self.ffi.new("short *")
        ret = self.lib.xiaGetParamData(self.channels[0].num, parname, value)
        check_ret(ret)
        return value[0]

    def read_base(self):
        t0 = time.perf_counter()
        base_data = []
        for channel in self.channels:
            chan_data = self.get_baseline(channel.num)
            base_data.append(chan_data)
        self.log.debug("handel read_base() took %3.2f" % (time.perf_counter() - t0))
        return base_data

    def set_acq_range(self, chan_min, chan_max):
        clen = chan_max - chan_min + 1

        # must be a multiple of 256
        #  round to the nearest multiple of 256
        clen = 256*round(clen/256) 
        self.set_acq_value("all","number_mca_channels", clen)
        self.apply_acq_values()

        self.spectrum_min = 0 
        self.spectrum_max = clen - 1
        self.spectrum_length = clen 

    def get_acq_range(self):
        return [self.spectrum_min, self.spectrum_max]

    def get_scas(self):
        return self.data_handler.get_all_counter_info(as_num=True)

    def set_scas(self, sca_defs):
      
        npars = len(sca_defs)
        nscas = math.floor( npars/4 )

        okpars = ( npars%4 == 0) and ( nscas > 0 )

        if not okpars:
            raise HandelError(PY_WRONG_SCA_DEFS)

        self.data_handler.init_counters()

        idx = 0
        cntno = 1

        while idx < npars: 
            chan, sca_low, sca_high, corr = sca_defs[idx:idx+4] 
            try:
                ichan = int(chan)
                for dxpchan in self.channels:
                    if ichan == dxpchan.num:
                        break
                else:
                    raise HandelError(PY_WRONG_SCA_DEFS,
                        errdesc="SCA channel does not match any DXP channel")
            except ValueError:
                # we could check here values like "all" and chan-range
                pass

            self.data_handler.add_counter(f"cnt{cntno}", chan, sca_low, sca_high, corr)

            cntno += 1
            idx += 4

        return nscas

    def reset_scas(self):
        self.data_handler.init_counters()

    @liblock
    def get_current_pixel(self, channel):
        curpx = self.ffi.new("unsigned int *")
        ret = self.lib.xiaGetRunData(channel, b"current_pixel", curpx)
        check_ret(ret)
        return curpx[0]

    @liblock
    def is_buffer_overrun(self, channel):
        isorun = self.ffi.new("unsigned int *")
        ret = self.lib.xiaGetRunData(channel, b"buffer_overrun", isorun)
        check_ret(ret)
        return isorun[0]
       
    @liblock
    def is_buffer_full(self, channel, bufidx):
        bufn = self.map_buffers[bufidx]

        isfull = self.ffi.new("unsigned int *")
        ret = self.lib.xiaGetRunData(channel,
                                     to_bytes("buffer_full_{}".format(bufn)),
                                     isfull)
        check_ret(ret)
        return isfull[0]

    @liblock
    def set_buffer_done(self, channum, bufidx):
        bufn = self.map_buffers[bufidx]
        cvalue = self.ffi.new("char *")

        ret = self.lib.xiaBoardOperation(channum,
                                         b"buffer_done",
                                         to_bytes(bufn))
        check_ret(ret)

    @liblock
    def read_map_buffer(self, channum, chanindex):
        mapbuf = self.map_raw_buffer[chanindex]

        data = self.ffi.cast("uint16_t *", mapbuf.ctypes.data)
        bufn = self.map_buffers[self.map_buffer_index]
        ret = self.lib.xiaGetRunData(channum,
                                     to_bytes('buffer_{}'.format(bufn)),
                                     data)
        check_ret(ret)

    def parse_map_buffers(self):
        nb_pix = 0

        elapsed0 = time.perf_counter() - self.time_first

        t0 = time.perf_counter()

        for chanindex, chan in enumerate(self.channels):
            buff = self.map_raw_buffer[chanindex]
            nb_pix = self.parse_map_one_chan(chanindex, buff)

        if nb_pix == 0:
            return

        per_px0 = 1000 * (elapsed0 / nb_pix)

        self.parse_map_calc_stats(nb_pix)


        self.data_handler.add_spectra(self.map_buf_data[:,0:nb_pix,:], 
                                      metadata=self.map_buf_stats[:,0:nb_pix,:])

        elapsed0 = time.perf_counter() - self.time_first 
        elapsed_handling = time.perf_counter()-t0

        per_pxhand = 1000 * (elapsed_handling / nb_pix)


        print(f"     +  cycle of {nb_pix} pixels took {elapsed0:3.3f} secs. {per_px0:>3.2f} msecs per pixel")
        print(f"           handling buffer took {elapsed_handling:3.3f} secs. {per_pxhand:>3.2f} msecs per pixel")

        self.time_first = time.perf_counter()
        self.map_read_pixel += nb_pix


    def parse_map_one_chan(self, chno, bf):

        ptr = 0

        header = bf[:128]
        header.dtype = np.uint16
        h0,h1,h2,h3 = header[0:4]

        assert (h0 == 0x55AA and h1 == 0xAA55)

        hsize = h2
        mapmode = h3

        h8,h9,h10,h11 = header[8:12]

        nb_pixels = h8
        start_px = dword(h9, h10)

        # advance header
        ptr += 128

        # attention to logic with pointers 
        #   to avoid multiple casting
        #      ptr works in 32bit
        #      pixel_header works in 16bit on same data

        for pixno in range(nb_pixels):
            # pxh = pixel header (is 128 32bits) but we
            #  only use 20
            pxh = bf[ptr:ptr+20]
            pxh.dtype = np.uint16 
            pxno = self.map_read_pixel + pixno

            assert pxh[0] == 0x33CC and pxh[1] == 0xCC33
            assert pxh[2] == 0x100

            block_size = bf[ptr+3] # pxh[6 and 7]
            spectrum_size = pxh[8]
    
            # operations with stats could be
            real_t = bf[ptr+16] #  pxh[32 and 33]
            live_t = bf[ptr+17] #  pxh[34 and 35]
            trigs = bf[ptr+18]  #  pxh[36 and 37]
            outs = bf[ptr+19]   #  pxh[38 and 39]

            self.map_raw_stats[chno, pixno, 0:6] = \
                          real_t,live_t,trigs,outs,chno

            # advance pixel header
            ptr+=128
            end_ptr = ptr+int(spectrum_size/2.0)
            spectrum_data = bf[ptr:end_ptr]
            #spectrum_data.dtype=np.uint32
            self.map_buf_data[chno, pixno,:] = spectrum_data
            ptr = end_ptr

        return nb_pixels

    def parse_map_calc_stats(self, nb_pixels):
        raw = self.map_raw_stats[:,0:nb_pixels,:]
        realt = raw[:,:,0]*CLOCK_TICK
        livet = raw[:,:,1]*CLOCK_TICK
        trig = raw[:,:,2]
        outp = raw[:,:,3]
        chns = raw[:,:,4]

        icr = np.divide(trig, livet, where=livet!=0)
        ocr = np.divide(outp, realt, where=realt!=0)

        # deadtime
        deadt = np.zeros_like(ocr)
        deadt.fill(0) 
        deadt = np.divide(icr - ocr, icr, where=icr!=0) 
        deadt[deadt<0] = 0

        evlivet = np.divide(outp, icr, where=icr!=0)
        dtcor = np.divide(icr, ocr, where=ocr!=0)

        self.map_buf_stats[:,0:nb_pixels,0] = chns
        self.map_buf_stats[:,0:nb_pixels,1] = outp
        self.map_buf_stats[:,0:nb_pixels,2] = icr
        self.map_buf_stats[:,0:nb_pixels,3] = ocr
        self.map_buf_stats[:,0:nb_pixels,4] = livet
        self.map_buf_stats[:,0:nb_pixels,5] = deadt

        self.map_buf_stats[:,0:nb_pixels,6] = realt
        self.map_buf_stats[:,0:nb_pixels,7] = trig
        self.map_buf_stats[:,0:nb_pixels,8] = evlivet
        self.map_buf_stats[:,0:nb_pixels,9] = dtcor



    ###  READING DATA ###
    def read_map_data(self, chno):
        return self.map_buf_data[chno,:,:]

    def read_scas(self, frompx=0, flatten=None):
        return self.data_handler.get_counter_values(frompx=frompx, flatten=flatten)

    def read_spectra_data(self, from_buffer=False):
        spectra = None
        for chan in self.channels:
            chdata = self.read_one_spectrum(chan.num)
            if spectra is None:
                spectra = chdata
            else:
                spectra = np.vstack([spectra,chdata])

        if len(spectra.shape) == 1:
            # only one spectrum. reshape to fit multichannel
            spectra = spectra.reshape(1, spectra.shape[0])
        return spectra

    @liblock
    def read_one_spectrum(self, channo):
        array = np.zeros(self.spectrum_length, dtype="uint32")
        data = self.ffi.cast("uint32_t *", array.ctypes.data)
        ret = self.lib.xiaGetRunData(channo, b'mca', data)
        check_ret(ret)
        return array

    def read_spectra_stat(self, from_buffer=False):
        if self.detector_mode == DetectorMode.MAP:
            return []
        else:
            sdata = [self.read_module_stat(module) \
                         for module in self.modules ]
            return np.concatenate( sdata )

    @liblock
    def read_module_stat(self, module):
        if self.xia_model == MODEL_FALCONX:  
            stat_length = 9 * module.number_of_channels
            cmd = b'module_statistics_2'
        else:
            # stat_length = 7 * module.number_of_channels
            stat_length = 7 * 4
            cmd = b'module_statistics'

        master = module.master

        array = np.zeros(stat_length, dtype="double")
        data = self.ffi.cast("double *", array.ctypes.data)
        ret = self.lib.xiaGetRunData(master, cmd, data)
          
        check_ret(ret)

        vals = []
        for ch in module.channels:
            chidx = ch.index_in_mod * 9
            realtime = array[chidx]
            livetime = array[chidx+1]
            triggers = array[chidx+3]
            events = array[chidx+4]
            icts = array[chidx+5]
            octs = array[chidx+6]
            if icts > 0:
               ins = (1.0 - octs/icts)
            else:
               ins = 0

            evlivet = np.divide(events, icts, where=icts!=0)
            dtcor = np.divide(icts, octs, where=octs!=0)

            vals.extend( [ch.num, events, icts, octs, livetime, ins, realtime, triggers, evlivet, dtcor] )
        
        return vals

    def read_map_stats(self, from_pixel=None):
        if from_pixel is None:
            from_pixel = 0

        nb_pix = self.map_read_pixel - from_pixel
        if nb_pix <= 0:
            raise HandelError(PY_NO_DATA)

        metadata = self.data_handler.get_metadata()
        return metadata[:,from_pixel:self.map_read_pixel,:]

    ###  END READING DATA ###

    def start_run(self, channel=None, resume=False):
        if channel is None:
            channel = -1 # all channels

        self.state = STATE_RUNNING
        self.do_start_run(channel, resume)

        runs = self.wait_running()
        time.sleep(0.05)

        if runs:
            self.started = True
            self.run_started = True
            self.last_update = time.time()
        else:
            self.state = STATE_READY
            self.started = False
            self.run_started = False
            raise HandelError(PY_CANNOT_START,
                                  errdesc="Cannot start run. Timeout (0.3) secs waiting for state to be running")

    @liblock
    def do_start_run(self,channel,resume):
        t0 = time.perf_counter()
        ret = self.lib.xiaStartRun(channel, resume)
        elapsed = time.perf_counter() -t0
        print(f'   - started - elapsed to start {elapsed:3.3f} secs')
        check_ret(ret)

    def stop_run(self, channel=None):
        try:
            if channel is None:
                channel = -1
            self.do_stop_run(channel)
        except BaseException as e:
            print(f"stop failed {str(e)}")

    @liblock
    def do_stop_run(self,channel):
        t0 = time.perf_counter()
        ret = self.lib.xiaStopRun(channel)
        elapsed = time.perf_counter()-t0
        print(f'   - stopped - elapsed to stop  {elapsed:3.3f} secs')
        check_ret(ret)

    @liblock
    def log_output_enable(self,flag):
        if flag:
            self.lib.xiaEnableLogOutput()
        else:
            self.lib.xiaSuppressLogOutput()

    @liblock
    def set_logfile(self, logfile=None):
        if logfile:
            ret = self.lib.xiaSetLogFile(logfile.encode())
            check_ret(ret)

    @liblock
    def set_loglevel(self, loglevel):
        lvl = -1
        if isinstance(loglevel,str) and loglevel.upper() in HANDEL_LOGLEVEL:
           lvl = HANDEL_LOGLEVEL[loglevel.upper()]
        elif isinstance(loglevel,int) and loglevel in HANDEL_LOGLEVEL.values(): 
           lvl = loglevel

        if lvl == -1:
           raise HandelError(PY_WRONG_LOGLEVEL)

        ret = self.lib.xiaSetLogLevel(lvl)
        check_ret(ret)

    def update(self):
        print("   - starting update loop")

        self.do_stop = False
        self.auto_update = True

        # sometimes we start but error when reading current_pixel
        #    handel complains errorno=11 is not yet active.   
        #  This sleeps aims at letting the system to really start. But
        # if the active state can be read from hw it should be implemented by
        # checking that one
        #    now we relay of wait_running() in start_run() to continue
        # time.sleep(self.refresh_rate+0.3)

        time.sleep(self.refresh_rate)

        while True:
            try:
                if self.do_stop:
                     print("   - stopping update loop")
                     self.auto_update = False
                     break
    
                with self._lock:
                    self._update_state()

                if self.state is not STATE_RUNNING:
                    break
    
                time.sleep(self.refresh_rate)
            except BaseException as e:
                import traceback
                traceback.print_exc()
                print("Exception is %s" % str(e))

        # make a last reading to make sure that all data is sent to data handler
        time.sleep(self.refresh_rate)

        try:
            self.update_data(forced=True, running=False)
        except BaseException as e:
            import traceback
            traceback.print_exc()
            print("Exception is %s" % str(e))
    
        self.data_handler.data_completed()
        
        print("   - update loop stopped")
        print("   - stopping detector...")
        #if not self.do_stop:
        print("   - stopping at the end of loop...")
        self.stop_run()

        self.auto_update = False
        self.started = False

    def start_update_loop(self):
        # starts thread for update
        self._lock = threading.Lock()
        self.t1 = threading.Thread(target=self.update, daemon=True)
        self.t1.start()

def dword(a1,a2):
    return a1 | a2 << 16

def to_bytes(var):
    return isinstance(var,bytes) and var or var.encode()

def check_ret(ret):
    if ret != 0:
        raise HandelError(errno=ret)

def main():
    return Handel(config_file="bixente2048.ini", 
                  config_path="c:\\blissadm\\falconx_config", 
                  lib_path="handel-sitoro-fxn-1.1.22-x64")

if __name__ == '__main__':
    main()
