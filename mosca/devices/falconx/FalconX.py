# -*- coding: utf-8 -*-
"""

FalconX python tango device server code

(code inspired from taco XMAP_DS device server code

Author: Vicente Rey

Description:

"""

from tango.server import Device, DeviceMeta, run
from tango.server import command, attribute
from tango.server import device_property

from tango import DevState, GreenMode
from tango import Util

from mosca.devices.falconx.pyhandel import Handel
from mosca.devices.falconx.pyhandel import DetectorMode
from mosca.devices.falconx.pyhandel import STATE_INIT, STATE_READY, STATE_RUNNING, STATE_UNKNOWN
from mosca.devices.falconx.pyhandel import MapMode
from mosca.devices.falconx.pyhandel import MODEL_FALCONX

from mosca.BaseSpectroDS import BaseSpectroDS
from mosca.Spectrometer import TriggerMode, PresetMode, Saving

""" see how to include git version here"""
__version__ = "0.1.b3"

def get_version():
    return  __version__

class FalconX(BaseSpectroDS):
    __metaclass__ = DeviceMeta

    UPDATE_TIME = 1
    UPDATE_WHEN_RUNNING = 0.1

    is_multichannel = True

    # green_mode = GreenMode.Asyncio

    lib_dir = device_property(dtype=str, 
                default_value='c:\\blissadm\\falconx\\handel-sitoro-fxn-1.1.22-x64\\lib')
    lib_name = device_property(dtype=str, default_value='handel.dll')
    config_dir = device_property(dtype=str, default_value='c:\\blissadm\\falconx_config')
    config_file = device_property(dtype=str, default_value='falconx.ini')

    gate_master = device_property(dtype=int)
    sync_master = device_property(dtype=int)
    input_polarity = device_property(dtype=int)

    logfile = device_property(dtype=str)
    loglevel = device_property(dtype=int)

    STATES = {
       STATE_INIT: DevState.INIT,
       STATE_READY: DevState.ON,
       STATE_RUNNING: DevState.RUNNING,
       STATE_UNKNOWN: DevState.UNKNOWN,
    }

    def init_device(self):
        super(FalconX, self).init_device()

        self.handel = None   

        self._info = "XIA FalconX"
        self._model = "XIA FalconX"

        self._trigger_modes = [
            TriggerMode.SOFTWARE,
            TriggerMode.SYNC,
            TriggerMode.GATE,
        ]

        self._trigger_mode = TriggerMode.SOFTWARE

        self._preset_modes = [
            PresetMode.NONE, 
            PresetMode.REALTIME, 
            PresetMode.LIVETIME,
            PresetMode.EVENTS,
            PresetMode.TRIGGERS, 
        ]

        """
PENDING:
    {DevXiaReadScas,    dev_readscas,       D_SHORT_TYPE,   D_VAR_ULONGARR},
    {DevXiaReadMapData, dev_readmapdata,    D_SHORT_TYPE,   D_VAR_ULONGARR},
    {DevXiaSave,        dev_save,           D_VOID_TYPE,    D_SHORT_TYPE},
"""
        kwargs = {}
        #if not self.config_dir:
            #self.config_dir = "c:\\blissadm\\falconx_config"
        kwargs['config_path'] = self.config_dir

        #if not self.config_file:
            #self.config_file =  "xmap.ini"
        kwargs['config_file'] = self.config_file

        if not self.lib_dir:
            self.lib_dir = "c:\\blissadm\\falconx\\handel-sitoro-fxn-1.1.22-x64\\lib"
        kwargs['lib_path'] = self.lib_dir

        if not self.lib_name:
             self.lib_name = "handel.dll"
        kwargs['lib_name'] = self.lib_name

        self.set_init_state()
        self.handel = Handel(**kwargs)
        self.handel.log_output_enable(True)

    def always_executed_hook(self):
        self.update_state()

    def update_state(self):
        if self.handel is None:
            self.set_init_state()
        else:
            state = self.handel.get_state() 
            self.set_state( self.state_to_devstate(state) )

    def state_to_devstate(self,state):
        return self.STATES.get(state,DevState.UNKNOWN)
         
    # 
    # Functions called by BaseSpectroDS device server API
    # 
    def get_info(self):
        return self.handel.get_info()

    def get_model(self):
        return self._model

    def get_number_channels(self):
        return self.handel.get_number_channels()

    def get_spectrum_size(self):
        return self.handel.get_spectrum_length()

    def get_preset_value(self):
        return self.handel.get_preset_value()

    def set_preset_value(self, value):
        return self.handel.set_preset_value(int(value))

    def get_preset_mode(self):
        mode_str = self.handel.get_preset_mode(as_string=True)
        if mode_str == "none":
            return PresetMode.NONE
        elif mode_str == "realtime":
            return PresetMode.REALTIME
        elif mode_str == "livetime":
            return PresetMode.LIVETIME
        elif mode_str == "events":
            return PresetMode.EVENTS
        elif mode_str == "triggers":
            return PresetMode.TRIGGERS

    def set_preset_mode(self, mode):
        if mode == PresetMode.NONE:
            self.handel.set_preset_mode("none")
        elif mode == PresetMode.REALTIME:
            self.handel.set_preset_mode("realtime")
        elif mode == PresetMode.LIVETIME:
            self.handel.set_preset_mode("livetime")
        elif mode == PresetMode.EVENTS:
            self.handel.set_preset_mode("events")
        elif mode == PresetMode.TRIGGERS:
            self.handel.set_preset_mode("triggers")

    def start_acquisition(self):
        self.set_running()
        self.handel.xia_start()

    def stop_acquisition(self):
        self.handel.xia_stop()

    def set_number_points(self, value):
        self._number_points = value
        if self.handel.get_detector_mode() == DetectorMode.MAP:
            self.handel.set_map_nb_pixels(value)

    def get_number_points(self):
        if self.handel.get_detector_mode() == DetectorMode.MCA:
            return self._number_points
        else:
            return self.handel.get_map_nb_pixels()

    def get_hw_current_pixel(self):
        if self.handel.get_detector_mode() == DetectorMode.MAP:
            return self.handel.get_map_current_pixel()
        else:
            return BaseSpectroDS.get_acquired_points(self)

    def set_trigger_mode(self,value):
        if value is TriggerMode.SOFTWARE:
            self.handel.set_detector_mode(mode=DetectorMode.MCA)
        elif value is TriggerMode.SYNC:
            self.handel.set_detector_mode(mode=DetectorMode.MAP, gate_ignore=1)
        elif value is TriggerMode.GATE:
            self.handel.set_detector_mode(mode=DetectorMode.MAP, gate_ignore=0)

    def get_trigger_mode(self):
        detector_mode = self.handel.get_detector_mode()
        gate_mode = self.handel.get_gate_mode()

        if detector_mode == DetectorMode.MCA:
            return TriggerMode.SOFTWARE
        elif gate_mode == MapMode.GATE: 
            return TriggerMode.GATE
        elif gate_mode == MapMode.SYNC: 
            return TriggerMode.SYNC

    #
    # IMPLEMENT Xia Taco interface compatibility 
    #
    @command
    def DevXiaStart(self):
        self.start_acquisition()

    @command
    def DevXiaStop(self):
        self.stop_acquisition()

    @command
    def DevXiaInit(self):
        self.handel.xia_init()

    @command(doc_in="""Set flag to clear on next start. 
    Not applicable for mapping acquisition mode.
    This flag is meaningful only if clear is not selected with DevXiaSetAcqPar
    """)
    def DevXiaClear(self):
        self.handel.set_clear_next()

    @command(dtype_in="int32",
            doc_in="""
            Set preset value for MCA acquisition
            For preset_mode realtime, livetime, the value must
            be provided in millisecs
            """)
    def DevXiaPreset(self,value):
        self.set_preset_value(value)

    @command(dtype_out='DevVarLongArray')
    def DevXiaGetChanPar(self):
        return self.handel.get_channel_pars()

    @command(dtype_out="DevVarLongArray")
    def DevXiaGetChanDef(self):
        return self.handel.get_channel_defs()

    @command(dtype_out="DevVarLongArray")
    def DevXiaGetAcqPar(self):
        return self.handel.get_acq_pars()

    @command(dtype_in="DevVarLongArray",
            doc_in="""
            Set acquisitin parameters
            argin[0] = detector_mode / MCA=1, MAP=4
            For MCA detector_mode
              argin[1] = use_gate: 1, gate_ignore: 0
              argin[2] = clear - Autoclear: 1, No clear: 0
              argin[3] = preset_mode - 
                  none = 0
                  realtime = 1
                  livetime = 2
                  output_cnts = 3
                  input_cnts = 4
            For MAP detector_mode
              argin[1] = ctrl, gate_ignore: 0 other sync_count=ctrl
              argin[2] = npixels to acquire
              argin[3] = npixels per buffer
            """)
    def DevXiaSetAcqPar(self, values):
        value0 = values[0]

        if value0 & DetectorMode.MAP.value:  
            det_mode = DetectorMode.MAP
        elif value0 & DetectorMode.MCA.value:
            det_mode = DetectorMode.MCA
        else:
            det_mode = None

        mode = self.handel.set_detector_mode(det_mode)

        if det_mode == DetectorMode.MCA:  
            gate, clear, preset_mode = values[1:] 
            self.handel.set_mca_acq_pars(gate, clear, preset_mode)
        elif det_mode == DetectorMode.MAP:  
            ctrl, npixels, nbuffer = values[1:] 
            self.handel.set_map_acq_pars(ctrl, npixels, nbuffer)

    @command(dtype_in="DevVarLongArray")
    def DevXiaSetAcqRange(self, argin):
        self.handel.set_acq_range(argin[0], argin[1])

    @command(dtype_out="DevVarLongArray")
    def DevXiaGetAcqRange(self):
        return self.handel.get_acq_range()

    @command(dtype_in=str, doc_in="""
            Name of config file to use
            Can be a either a full path 
               or 
            a file in the folder set by the "config_dir" property.
            A default "config_file" can be set also by setting the property
            "config_file".
            """)
    def DevXiaSetConfig(self, filename):
        self.handel.load_config(filename)

    @command(dtype_out=str,
            doc_out="""
            Name of the configuration file currently being used
            """)
    def DevXiaGetConfig(self):
        filename = self.handel.get_config_filename()
        return filename

    @command(dtype_out="DevVarStringArray")
    def DevXiaGetConfigList(self):
        return self.handel.get_config_filelist()

    @command(dtype_out=str)
    def DevXiaGetVersion(self):
        return get_version()

    @command(dtype_out="DevVarStringArray")
    def DevXiaGetScas(self):
        return self.handel.get_scas()

    @command(dtype_in="DevVarLongArray", dtype_out="int32")
    def DevXiaSetScas(self,sca_defs):
        return self.handel.set_scas(sca_defs)

    @command(dtype_in="int32", dtype_out="DevVarULongArray")
    def DevXiaReadScas(self, frompx):
        return self.data_handler.get_counter_values(frompx=int(frompx), flatten="pixel")

    @command
    def DevXiaResetScas(self):
        return self.data_handler.init_counters()

    @command(dtype_out="DevVarULongArray")
    def DevXiaReadBase(self):
        return self.handel.read_base()

    # @command(dtype_in=str)
    # def DevXiaGetDspPar(self, parname):
    #     return self.handel.get_dsp_par(parname)

    # @command(dtype_in="DevVarLongArray")
    # def DevXiaSetDspPar(self, values):
    #     """
    #    Set param values for each channel. Param to be set is the one 
    #   previously read with get_dsp_par()
    #    """
    #    return self.handel.set_dsp_par(values)

    # @command(dtype_in="int32", dtype_out="DevVarStringArray")
    # def DevXiaGetDspNames(self, chan_num):
    #     return self.handel.get_dsp_names(chan_num)

    # @command(dtype_in="int32", dtype_out="DevVarShortArray")
    # def DevXiaGetDspValues(self, chan_num):
    #    return self.handel.get_dsp_values(chan_num)

    @command(dtype_in="int32", dtype_out="DevVarLongArray")
    def DevXiaReadData(self, value):
        #  argin value:
        #    in mca_mode argin is ignored. originally was from_buffer yes/no
        #    in map_mode argin is first_pixel
        return self.data_handler.get_data(flatten=True)

    @command(dtype_in="int32", dtype_out="DevVarLongArray")
    def DevXiaReadStat(self, value):
        #  argin value:
        #    in mca_mode argin is ignored. originally was from_buffer yes/no
        #    in map_mode argin is first_pixel
        return self._get_taco_metadata()

    @command(dtype_in="DevShort", dtype_out="DevVarULongArray")
    def DevXiaReadMapData(self, detno):
        return self.data_handler.get_data()[detno,:,:].flatten()

    @command(dtype_in="int32", dtype_out="DevVarFloatArray")
    def DevXiaReadMapStat(self, value):
        #stats = self.handel.read_map_stats(from_pixel=value)
        # flatten making sure that stats vals are
        # ordered by pixel as required by spec macros
        return self._get_taco_metadata(from_px=value, orderby='pixel')
    
    def _get_taco_metadata(self, from_px=0, orderby='det'):
        data = self.data_handler.get_metadata(from_px=from_px, flatten=False, orderby=orderby).copy()
        if self.data_handler.multichannel:
            stats = data[:,:,:6]
            stats[:,:,4] *= 1000 # convert livetime to ms
            stats[:,:,5] *= 100 # convert deadtime to percent
            if orderby == 'pixel':
                stats = stats.swapaxes(0,1)
        else:
            stats = data[:,:6]
            stats[:,4] *= 1000 # convert livetime to ms
            stats[:,5] *= 100 # convert deadtime to percent
        
        return stats.flatten()

    @command(dtype_out="DevVarLongArray", 
             doc_out="""map state
             argout[0] = state - 0: READY, 2: RUNNING
             argout[1] = map_current_pixel 
             argout[2] = map_read_pixel
             argout[3] = map_file_saved
             """)
    def DevXiaGetMapState(self):
        return self.handel.get_map_state()

    @command(dtype_in="DevVarStringArray",
            doc_in="""file parameters for saving. 
            The default file name follows the scheme:
            {folder}/{prefix}_xiaNN_{num1}_{num2}_{num3}.edf
            argin[0] = folder
            argin[1] = prefix
            argin[2] = num1
            argin[3] = num2
            argin[4] = num3
            argin[5] = flags. bit 1: autosave, bit2: bidirectional
            """)
    def DevXiaSetFilePar(self, *pars):
        pars = pars[0]
        if len(pars) != 6:
            raise BaseException("Wrong number of arguments %d, expecting 6" % len(pars))

        folder, prefix = pars[0:2]
        filenums = pars[2:5]
        saveflags = int(pars[5])
        auto_save = (saveflags & 0x1) == 0x1
        reverse = (saveflags & 0x2) == 0x2

        filenums = list(map(int, filenums))

        self.data_handler.set_saving_directory(folder)
        self.data_handler.set_saving_prefix(prefix)
        self.data_handler.set_saving_xmap_numbers(filenums)
        self.data_handler.set_saving_reverse(reverse)
        if auto_save:
            self.data_handler.set_saving_mode(Saving.AUTO)
        else:
            self.data_handler.set_saving_mode(Saving.MANUAL)

        self.data_handler.reset_header()
        print(f' file pars set next path-> {self.data_handler.get_next_file_path()}')

    @command(dtype_out="DevVarStringArray")
    def DevXiaGetFilePar(self):

        reverse = self.data_handler.get_saving_reverse()
        autosave = self.data_handler.get_saving_mode() is Saving.AUTO

        folder = self.data_handler.get_saving_directory()
        print(f'folder: {folder}')
        prefix = self.data_handler.get_saving_prefix()
        print(f'prefix: {prefix}')
        nums = list(map(str,self.data_handler.get_saving_xmap_numbers()))
        print(f'nums: {nums}')
        flags = 0 
        if autosave and 0x1:
           flags |= 0x1 
        if reverse and 0x2:
           flags |= 0x2 
        flags = str(flags)

        print(f'flags: {flags} - autosave {autosave} / reverse {reverse}')

        return [folder, prefix] + nums + [flags]

    @command(dtype_in="DevVarStringArray")
    def DevXiaSetHeader(self, headers):
        for line in  headers:
            ky, val = line.split(" = ")
            self.data_handler.add_header(ky,val)

    @command(dtype_out="DevShort")
    def DevXiaSave(self):
        return self.data_handler.save()

    @command(dtype_in=bool, doc_in="""Enable (1) or disable (0) 
       LOG output for pyhandel library
       """)
    def DevXiaLogOutput(self, value):
        self.handel.log_output_enable(value)

    @command(dtype_in=str)
    def DevXiaSetLogFile(self, filename):
        self.handel.set_logfile(filename)

    @command(dtype_in=str, doc_in="""Selects the LOG level for
        selecting messages. Possible values are:
          WARNING, DEBUG, INFO or ERROR
        """)
    def DevXiaSetLogLevel(self, level):
        self.handel.set_loglevel(level)

    #
    # ATTRIBUTES
    #

    @attribute(dtype=str)
    def config(self):
        return self.handel.get_config_filename()

    @config.write
    def config(self, filename):
        self.handel.load_config(filename)
        self.config_update()

    def config_update(self):
        if self.handel.get_model() == MODEL_FALCONX:
            # livetime preset_mode is not supported by Falcon
            # remove it from preset_modes. 
            #   no need to restore as model will not change
            try:
                idx = self._preset_modes.index(PresetMode.LIVETIME)
                self._preset_modes.pop(idx)
            except ValueError:
                pass
                 
    @attribute(dtype=str)
    def config_path(self):
        return self.handel.get_config_path()

    @config_path.write
    def config_path(self, path):
        self.handel.set_config_path(path)

    @attribute(dtype=[str], max_dim_x=100)
    def channels_module_and_index(self):
        return self.handel.get_channels_module_and_index()

    @attribute(dtype=[int], max_dim_x=100)
    def channels_alias(self):
        return self.handel.get_channels_alias()
    
    @attribute(dtype=float)
    def refresh_rate(self):
        return self.handel.get_refresh_rate()

    @refresh_rate.write
    def refresh_rate(self,value):
        return self.handel.set_refresh_rate(value)

    @attribute(dtype=str)
    def detector_mode(self):
        detector_mode = self.handel.get_detector_mode()
        return detector_mode.name

    @detector_mode.write
    def detector_mode(self, value):
        isvalue = isname = False
        try:
            val = int(value)
            isvalue = True
        except ValueError:
            isname = True
            value = value.upper()

        for mode in DetectorMode:
            if isvalue and mode.value == val:
                detector_mode = mode
                break
            if isname and mode.name.startswith(value):
                detector_mode = mode
                break
        else:
            raise BaseException("detector mode not supported")

        self.handel.set_detector_mode(detector_mode)
        if detector_mode == DetectorMode.MAP:
            self.set_number_points(self._number_points)

    @attribute(dtype=bool)
    def clear_next(self):
        return self.handel.get_clear_next()

    @clear_next.write
    def clear_next(self,value):
        return self.handel.set_clear_next(value)

    @attribute(dtype=bool)
    def gate_ignore(self):
        return self.handel.get_gate_ignore()

    @gate_ignore.write
    def gate_ignore(self,value):
        return self.handel.set_gate_ignore(value)

    @attribute(dtype=("int32",), max_dim_x=2)
    def acq_range(self):
        arange = self.handel.get_acq_range()
        return arange

    @acq_range.write
    def acq_range(self,acqrange):
        self.handel.set_acq_range(*acqrange)

    @attribute(dtype="str")
    def map_pixel_mode(self):
        return self.handel.get_map_pixel_mode()
        
    # @map_pixel_mode.write
    # def map_pixel_mode(self, value):
    #     self.handel.set_map_pixel_mode(value)

    @attribute(dtype="int32")
    def map_number_pixels(self):
        return self.handel.get_map_nb_pixels()

    @map_number_pixels.write
    def map_number_pixels(self, value):
        return self.handel.set_map_nb_pixels(value)

    @attribute(dtype="int32")
    def map_pixels_per_buffer(self):
        return self.handel.get_map_pixels_per_buffer()
    
    @map_pixels_per_buffer.write
    def map_pixels_per_buffer(self, value):
        return self.handel.set_map_pixels_per_buffer(value)

    @attribute(dtype="int32")
    def map_current_pixel(self):
        return self.handel.get_map_current_pixel()

    @attribute(dtype="int32")
    def map_read_pixel(self):
        return self.handel.get_map_read_pixel()


def run_it():
    run((FalconX,))

if __name__ == '__main__':
    run_it()

