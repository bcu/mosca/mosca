from mosca.devices.falconx.import_hdef import import_hdef
import os

def_incdir = os.path.join(os.path.dirname(__file__), "handel_inc")
include_dir = os.environ.get("HANDEL_INC_PATH", def_incdir)

# import all "#define" symbols from the handel_errors.h file
# by default the files are distributed with the pyhandel module. 
#   on installation the HANDEL_INC_PATH could point to a different directory

HANDEL_ERRORS = import_hdef(os.path.join(include_dir, "handel_errors.h"), update_module=__name__, error_mode=True)

