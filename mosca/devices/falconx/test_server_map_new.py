
from tango import DeviceProxy

import time
import sys

config_file = "double_a_c.ini"

points = int(sys.argv[1])
buffer_size = int(sys.argv[2])

dev = DeviceProxy("id00/falconx/txo")
#dev.DevXiaStart()

# dev.detector_mode = "MAP"
# dev.map_number_pixels = points
# dev.map_pixels_per_buffer = buffer_size
  # set map pars does more than simply changing values. to be revisited
dev.DevXiaSetAcqPar([5,0,points,buffer_size])
dev.startAcq()

# ctrl
print(f"Starting xia in map mode for {points} points - buffer_size: {buffer_size} ")
time.sleep(1.5)

try:
    while str(dev.State()) != 'RUNNING':
        print(f". {dev.State()}")
        time.sleep(0.5)
    print("xia started")
    while str(dev.State()) == 'RUNNING':
        print(f". {dev.State()}")
        time.sleep(1)
    print("xia acq done")
except KeyboardInterrupt:
    #dev.DevXiaStop()
    dev.stopAcq()
