
from tango import DeviceProxy, DevState
import time
import os
import sys

os.environ["TANGO_HOST"]="lid00ctrl:20000"

dp = DeviceProxy("id00/falconx/txo")
dp.DevXiaSetConfig("cyril6.ini")
dp.DevXiaSetConfig("double_a_c.ini")

dp.preset_mode = "realtime"
dp.DevXiaSetAcqPar([1,0,1,1])
print(dp.DevXiaGetAcqPar())

dp.preset_value = 2000

dp.DevXiaStart()
print( dp.State() )

while dp.State() == DevState.RUNNING:
    print(".")
    time.sleep(0.2)

print()
print("acq. finished")

data = dp.DevXiaReadData(0)
stat = dp.DevXiaReadStat(0)
chdef = dp.DevXiaGetChanDef()
nb_channels = len(chdef)

print("Nb of values = %d" % len(data))
print("   max value = %d" % data.max())

print("Statistics:")
print("  #   nb-events      icts      octs   livet  ics%% ")
for ch in range(nb_channels):
   idx = ch*6
   nb, events, icts, octs, livet, ics = stat[idx:idx+6]
   print(" %2d   %8d   %8d  %8d   %5d   %2d" % (nb,events,icts,octs,livet,ics))


