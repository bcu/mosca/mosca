
from tango import DeviceProxy

import time
import sys

config_file = "Lagaffe_ch123_Gaston_ch45678_MediumRate_3072ch_27092022.ini"

nb_loops = int(sys.argv[1])
points = int(sys.argv[2])
buffer_size = int(sys.argv[3])

dev = DeviceProxy("id16b/falconx/1")
dev.DevXiaSetConfig(config_file)
dev.saving_mode = "MANUAL"


def wait_loop(i, dev):
   
    while str(dev.State()) != 'RUNNING':
        print("  - waiting.. state:%s" % str(dev.State))
        time.sleep(0.5)
    print("xia started")
    read_pixel = read_pixels_old = 0
    expected_pixels = dev.map_number_pixels
    print(f"        / read: {read_pixel: 4d}/{expected_pixels:4d}\r", end="")
    t0 = None
    while str(dev.State()) == 'RUNNING':
        read_pixel = dev.map_read_pixel
        current_pixel = dev.map_current_pixel
        time.sleep(0.1)
        if current_pixel != read_pixels_old:
             if current_pixel != 0 and t0 is None:
                 t0 = time.time()
                 fpx = current_pixel
             print(f"        / read: {current_pixel: 4d}/{expected_pixels:4d}\r", end="")
             read_pixels_old = current_pixel
    time.sleep(1)
    read_pixel = dev.map_read_pixel
    print(f"       / read: {read_pixel: 4d}/{expected_pixels:4d} - ACQ DONE", end="\n")
    elapsed = time.time() - t0
    elap_px = 1000 * elapsed / (read_pixel - fpx) 
    print(f"     Iter {i+1} finished. elapsed: {elapsed:03.2f} secs, per pixel {elap_px:02.2f} msecs per pixel")

def loop():
    for  i in range(nb_loops):
        print(f"Acq no {i+1}")
        dev.DevXiaSetAcqPar([4,0,points,buffer_size])

        # dev.DevXiaSetFilePar(['vic3','test2',str(i),'0','0','1'])
        

        dev.DevXiaStart()

        # ctrl
        print(f"   - starting map mode for {points} points - buffer_size: {buffer_size} ")
        time.sleep(1)
        try:
            wait_loop(i, dev)
        except KeyboardInterrupt:
            dev.DevXiaStop()
        time.sleep(3)

if __name__ == '__main__':
     loop()
