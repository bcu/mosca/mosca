
from tango import DeviceProxy, DevState
import time
import os
import sys

os.environ["TANGO_HOST"]="lid00ctrl:20000"
dp = DeviceProxy("id00/falconx/txo")
dp.config_path = 'c:\\blissadm\\falconx_config'
dp.DevXiaSetConfig("double_a_c.ini")

dp.detector_mode = "mca"
dp.gate_ignore = True
dp.preset_mode = "realtime"
dp.preset_value = 2000

dp.startAcq()

while dp.State() == DevState.RUNNING:
    time.sleep(0.2)
    data = dp.data
    print(f" running. sum is {data.sum()}", end='\r')

data = dp.data
stat = dp.metadata
labels = dp.metadata_labels
nb_channels = dp.number_channels

print(f" done. sum is {data.sum()}    ", end='\n\n')

print("Nb of values = %d" % len(data))
print("   max value = %d" % data.max())

print("\n<STATS>")
header = "# Ch   " + " ".join([f"{label:>8s}" for label in labels[1:]])
print(header)
print("-"*len(header))
for ch in range(nb_channels):
   idx = ch*6
   nb, events, icts, octs, livet, ics = stat[idx:idx+6]
   print(f" {nb:>3d}   {events:>8d} {icts:>8d} {octs:>8d} {livet:>8d} {ics:>8d}")


