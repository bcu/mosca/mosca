
# MoSCa - Modules for Spectra Capture

MoSCa is a set of python modules for the integration of 1D fluorescence
detectors and other 1D detectors into a laboratory control system.

## Description

This set of modules offer a common interface and features for any 1D detector
being integrated.

## Generic features

## API - Application program interface

###  Tango
###  SPEC
###  BLISS
